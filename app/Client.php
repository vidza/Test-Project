<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $primaryKey="project_id";
    protected $table='clients';
    protected $guarded = [];
}
