<?php

namespace App\Helperss;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class Helperss
{

    public static function in_array_field($needle, $needle_field, $haystack, $strict = false)
    {
        $in = false;
        if ($strict) {
            foreach ($haystack as $item)
                if (!empty($item[$needle_field]) || !empty($item->$needle_field))
                    if ($item[$needle_field] === $needle)

                        $in = true;
        } else {
            foreach ($haystack as $item)
                if (isset($item->$needle_field) && $item->$needle_field == $needle)
                    $in = true;
        }
        return $in;
    }

    public static function startEndDate($status = null, $start_date = null, $end_date = null, $first_array = null, $second_array = null)
    {

        $time_entries_array = null;
        $i = 0;
        if ($status == 'project user') {

            if ($start_date != null or $end_date != null) {

                if ($start_date != null and $end_date != null) {

                    $start = substr(Carbon::createFromFormat('m/d/Y', $start_date), 0, 10);
                    $end = substr(Carbon::createFromFormat('m/d/Y', $end_date)->addDay(1), 0, 10);

                    foreach ($second_array as $user) {

                        foreach ($first_array as $project) {

                            $time_entries_array[$i++] = DB::table('time_entries')
                                ->join('users', 'fk_for_user', '=', 'user_id')
                                ->join('projects', 'fk_for_projects', '=', 'project_id')
                                ->where('fk_for_user', $user)
                                ->where('fk_for_projects', $project)
                                ->whereBetween('start_time', [$start, $end])
                                ->orderBy('projects.project_name')
                                ->orderBy('time_entries.start_time')
                                ->select('users.user_fname',
                                    'users.user_lname',
                                    'users.user_id',
                                    'projects.project_id',
                                    'time_entries.time_entry_id',
                                    'time_entries.start_time',
                                    'time_entries.duration',
                                    'time_entries.description',
                                    'time_entries.status_git',
                                    'projects.project_name'
                                )
                                ->get();
                        }
                    }
                } else if ($start_date != null) {
                    $start = substr(Carbon::createFromFormat('m/d/Y', $start_date), 0, 10);
                    foreach ($second_array as $user) {

                        foreach ($first_array as $project) {

                            $time_entries_array[$i++] = DB::table('time_entries')
                                ->join('users', 'fk_for_user', '=', 'user_id')
                                ->join('projects', 'fk_for_projects', '=', 'project_id')
                                ->where('fk_for_user', $user)
                                ->where('fk_for_projects', $project)
                                ->where('start_time', '>', $start)
                                ->orderBy('projects.project_name')
                                ->orderBy('time_entries.start_time')
                                ->select('users.user_fname',
                                    'users.user_lname',
                                    'users.user_id',
                                    'projects.project_id',
                                    'time_entries.time_entry_id',
                                    'time_entries.start_time',
                                    'time_entries.duration',
                                    'time_entries.description',
                                    'time_entries.status_git',
                                    'projects.project_name'
                                )
                                ->get();
                        }
                    }
                } else if ($end_date != null) {

                    $end = substr(Carbon::createFromFormat('m/d/Y', $end_date)->addDay(1), 0, 10);

                    foreach ($second_array as $user) {

                        foreach ($first_array as $project) {

                            $time_entries_array[$i++] = DB::table('time_entries')
                                ->join('users', 'fk_for_user', '=', 'user_id')
                                ->join('projects', 'fk_for_projects', '=', 'project_id')
                                ->where('fk_for_user', $user)
                                ->where('fk_for_projects', $project)
                                ->where('start_time', '<', $end)
                                ->orderBy('projects.project_name')
                                ->orderBy('time_entries.start_time')
                                ->select('users.user_fname',
                                    'users.user_lname',
                                    'users.user_id',
                                    'projects.project_id',
                                    'time_entries.time_entry_id',
                                    'time_entries.start_time',
                                    'time_entries.duration',
                                    'time_entries.description',
                                    'time_entries.status_git',
                                    'projects.project_name'
                                )
                                ->get();
                        }
                    }
                }

            } else {

                foreach ($second_array['user'] as $user) {

                    foreach ($first_array['project'] as $project) {


                        $time_entries_array[$i++] = DB::table('time_entries')
                            ->join('users', 'fk_for_user', '=', 'user_id')
                            ->join('projects', 'fk_for_projects', '=', 'project_id')
                            ->where('fk_for_user', $user)
                            ->where('fk_for_projects', $project)
                            ->orderBy('projects.project_name')
                            ->orderBy('time_entries.start_time')
                            ->select('users.user_fname',
                                'users.user_lname',
                                'users.user_id',
                                'projects.project_id',
                                'time_entries.time_entry_id',
                                'time_entries.start_time',
                                'time_entries.duration',
                                'time_entries.description',
                                'time_entries.status_git',
                                'projects.project_name'
                            )
                            ->get();
                    }
                }
            }

        } else if ($status == 'project') {


            if ($start_date != null or $end_date != null) {

                if ($start_date != null and $end_date != null) {

                    $start = substr(Carbon::createFromFormat('m/d/Y', $start_date), 0, 10);
                    $end = substr(Carbon::createFromFormat('m/d/Y', $end_date)->addDay(1), 0, 10);

                    foreach ($first_array as $project) {

                        $time_entries_array[$i++] = DB::table('time_entries')
                            ->join('users', 'fk_for_user', '=', 'user_id')
                            ->join('projects', 'fk_for_projects', '=', 'project_id')
                            ->where('fk_for_projects', $project)
                            ->whereBetween('start_time', [$start, $end])
                            ->orderBy('projects.project_name')
                            ->orderBy('time_entries.start_time')
                            ->select('users.user_fname',
                                'users.user_lname',
                                'users.user_id',
                                'projects.project_id',
                                'time_entries.time_entry_id',
                                'time_entries.start_time',
                                'time_entries.duration',
                                'time_entries.description',
                                'time_entries.status_git',
                                'projects.project_name'
                            )
                            ->get();
                    }

                } else if ($start_date != null) {
                    $start = substr(Carbon::createFromFormat('m/d/Y', $start_date), 0, 10);

                    foreach ($first_array as $project) {

                        $time_entries_array[$i++] = DB::table('time_entries')
                            ->join('users', 'fk_for_user', '=', 'user_id')
                            ->join('projects', 'fk_for_projects', '=', 'project_id')
                            ->where('fk_for_projects', $project)
                            ->where('start_time', '>', $start)
                            ->orderBy('projects.project_name')
                            ->orderBy('time_entries.start_time')
                            ->select('users.user_fname',
                                'users.user_lname',
                                'users.user_id',
                                'projects.project_id',
                                'time_entries.time_entry_id',
                                'time_entries.start_time',
                                'time_entries.duration',
                                'time_entries.description',
                                'time_entries.status_git',
                                'projects.project_name'
                            )
                            ->get();

                    }
                } else if ($end_date != null) {

                    $end = substr(Carbon::createFromFormat('m/d/Y', $end_date)->addDay(1), 0, 10);

                    foreach ($first_array as $project) {

                        $time_entries_array[$i++] = DB::table('time_entries')
                            ->join('users', 'fk_for_user', '=', 'user_id')
                            ->join('projects', 'fk_for_projects', '=', 'project_id')
                            ->where('fk_for_projects', $project)
                            ->where('start_time', '<', $end)
                            ->orderBy('projects.project_name')
                            ->orderBy('time_entries.start_time')
                            ->select('users.user_fname',
                                'users.user_lname',
                                'users.user_id',
                                'projects.project_id',
                                'time_entries.time_entry_id',
                                'time_entries.start_time',
                                'time_entries.duration',
                                'time_entries.description',
                                'time_entries.status_git',
                                'projects.project_name'
                            )
                            ->get();
                    }
                }

            } else {

                foreach ($first_array as $project) {


                    $time_entries_array[$i++] = DB::table('time_entries')
                        ->join('users', 'fk_for_user', '=', 'user_id')
                        ->join('projects', 'fk_for_projects', '=', 'project_id')
                        ->where('fk_for_projects', $project)
                        ->orderBy('projects.project_name')
                        ->orderBy('time_entries.start_time')
                        ->select('users.user_fname',
                            'users.user_lname',
                            'users.user_id',
                            'projects.project_id',
                            'time_entries.time_entry_id',
                            'time_entries.start_time',
                            'time_entries.duration',
                            'time_entries.description',
                            'time_entries.status_git',
                            'projects.project_name'
                        )
                        ->get();
                }
            }

        } else if ($status == 'user') {


            if ($start_date != null or $end_date != null) {

                if ($start_date != null and $end_date != null) {

                    $start = substr(Carbon::createFromFormat('m/d/Y', $start_date), 0, 10);
                    $end = substr(Carbon::createFromFormat('m/d/Y', $end_date)->addDay(1), 0, 10);

                    foreach ($first_array as $user) {

                        $time_entries_array[$i++] = DB::table('time_entries')
                            ->join('users', 'fk_for_user', '=', 'user_id')
                            ->join('projects', 'fk_for_projects', '=', 'project_id')
                            ->where('fk_for_user', $user)
                            ->whereBetween('start_time', [$start, $end])
                            ->orderBy('projects.project_name')
                            ->orderBy('time_entries.start_time')
                            ->select('users.user_fname',
                                'users.user_lname',
                                'users.user_id',
                                'projects.project_id',
                                'time_entries.time_entry_id',
                                'time_entries.start_time',
                                'time_entries.duration',
                                'time_entries.description',
                                'time_entries.status_git',
                                'projects.project_name'
                            )
                            ->get();
                    }

                } else if ($start_date != null) {
                    $start = substr(Carbon::createFromFormat('m/d/Y', $start_date), 0, 10);

                    foreach ($first_array as $user) {

                        $time_entries_array[$i++] = DB::table('time_entries')
                            ->join('users', 'fk_for_user', '=', 'user_id')
                            ->join('projects', 'fk_for_projects', '=', 'project_id')
                            ->where('fk_for_user', $user)
                            ->where('start_time', '>', $start)
                            ->orderBy('projects.project_name')
                            ->orderBy('time_entries.start_time')
                            ->select('users.user_fname',
                                'users.user_lname',
                                'users.user_id',
                                'projects.project_id',
                                'time_entries.time_entry_id',
                                'time_entries.start_time',
                                'time_entries.duration',
                                'time_entries.description',
                                'time_entries.status_git',
                                'projects.project_name'
                            )
                            ->get();

                    }
                } else if ($end_date != null) {

                    $end = substr(Carbon::createFromFormat('m/d/Y', $end_date)->addDay(1), 0, 10);

                    foreach ($first_array as $user) {

                        $time_entries_array[$i++] = DB::table('time_entries')
                            ->join('users', 'fk_for_user', '=', 'user_id')
                            ->join('projects', 'fk_for_projects', '=', 'project_id')
                            ->where('fk_for_user', $user)
                            ->where('start_time', '<', $end)
                            ->orderBy('projects.project_name')
                            ->orderBy('time_entries.start_time')
                            ->select('users.user_fname',
                                'users.user_lname',
                                'users.user_id',
                                'projects.project_id',
                                'time_entries.time_entry_id',
                                'time_entries.start_time',
                                'time_entries.duration',
                                'time_entries.description',
                                'time_entries.status_git',
                                'projects.project_name'
                            )
                            ->get();
                    }
                }

            } else {

                foreach ($first_array as $user) {


                    $time_entries_array[$i++] = DB::table('time_entries')
                        ->join('users', 'fk_for_user', '=', 'user_id')
                        ->join('projects', 'fk_for_projects', '=', 'project_id')
                        ->where('fk_for_user', $user)
                        ->orderBy('projects.project_name')
                        ->orderBy('time_entries.start_time')
                        ->select('users.user_fname',
                            'users.user_lname',
                            'users.user_id',
                            'projects.project_id',
                            'time_entries.time_entry_id',
                            'time_entries.start_time',
                            'time_entries.duration',
                            'time_entries.description',
                            'time_entries.status_git',
                            'projects.project_name'
                        )
                        ->get();
                }
            }

        }

        return $time_entries_array;


    }

    public static function convertSum($sum)
    {
        $result_string = '';
        $time = [];
        $hour = 3600;
        $minute = 60;

        if ($sum / $hour > 1) {

            $time['hour'] = round($sum / $hour);
            $result_string .= $time['hour'] . 'h ';
            $sum -= $time['hour'] * $hour;

        }
        if ($sum / $minute > 1) {
            $time['minute'] = round($sum / $minute);
            $result_string .= $time['minute'] . 'min ';
            $time['seconds'] = $sum - $time['minute'] * $minute;
            $result_string .= $time['seconds'] . 's';

        } else {
            $time['seconds'] = $sum;
            $result_string .= $time['seconds'] . 's';
        }

        return $result_string;
    }


    public static function detectWeek($dayOfWeek)
    {
        $time_array = [];

        switch ($dayOfWeek) {

            case"Monday":
                {

                    $time_array['start'] = substr(Carbon::createFromFormat('Y-m-d', Carbon::now('+2')->toDateString()), 0, 10);
                    $time_array['end'] = substr(Carbon::createFromFormat('Y-m-d', Carbon::now('+2')->toDateString())->addDays(7), 0, 10);

                    return $time_array;
                    break;
                }
            case"Tuesday":
                {
                    $time_array['start'] = substr(Carbon::createFromFormat('Y-m-d', Carbon::now('+2')->toDateString())->subDay(1), 0, 10);
                    $time_array['end'] = substr(Carbon::createFromFormat('Y-m-d', Carbon::now('+2')->toDateString())->addDays(6), 0, 10);

                    return $time_array;
                    break;
                }
            case"Wednesday":
                {

                    $time_array['start'] = substr(Carbon::createFromFormat('Y-m-d', Carbon::now('+2')->toDateString())->subDays(2), 0, 10);
                    $time_array['end'] = substr(Carbon::createFromFormat('Y-m-d', Carbon::now('+2')->toDateString())->addDays(5), 0, 10);

                    return $time_array;
                    break;
                }
            case"Thursday":
                {

                    $time_array['start'] = substr(Carbon::createFromFormat('Y-m-d', Carbon::now('+2')->toDateString())->subDays(3), 0, 10);
                    $time_array['end'] = substr(Carbon::createFromFormat('Y-m-d', Carbon::now('+2')->toDateString())->addDays(4), 0, 10);

                    return $time_array;
                    break;
                }
            case"Friday":
                {

                    $time_array['start'] = substr(Carbon::createFromFormat('Y-m-d', Carbon::now('+2')->toDateString())->subDays(4), 0, 10);
                    $time_array['end'] = substr(Carbon::createFromFormat('Y-m-d', Carbon::now('+2')->toDateString())->addDays(3), 0, 10);

                    return $time_array;
                    break;
                }
            case"Saturday":
                {

                    $time_array['start'] = substr(Carbon::createFromFormat('Y-m-d', Carbon::now('+2')->toDateString())->subDays(5), 0, 10);
                    $time_array['end'] = substr(Carbon::createFromFormat('Y-m-d', Carbon::now('+2')->toDateString())->addDays(2), 0, 10);

                    return $time_array;
                    break;
                }
            case"Sunday":
                {

                    $time_array['start'] = substr(Carbon::createFromFormat('Y-m-d', Carbon::now('+2')->toDateString())->subDays(6), 0, 10);
                    $time_array['end'] = substr(Carbon::createFromFormat('Y-m-d', Carbon::now('+2')->toDateString())->addDay(1), 0, 10);

                    return $time_array;
                    break;

                }


        }


    }

    public static function detectMonth()
    {
        $time_array = [];

        $time_array['start'] = substr(Carbon::createFromFormat('Y-m-d', Carbon::now('+2')->startOfMonth()->toDateString()), 0, 10);
        $time_array['end'] = substr(Carbon::createFromFormat('Y-m-d', Carbon::now('+2')->toDateString())->addDay(1), 0, 10);


        return $time_array;
    }

    public static function detectSummary()
    {

        $time_array = [];

        $time_array['start'] = substr(Carbon::createFromFormat('Y-m-d', '2017-01-01'), 0, 10);
        $time_array['end'] = substr(Carbon::createFromFormat('Y-m-d', Carbon::now('+2')->toDateString())->addDay(1), 0, 10);

        return $time_array;
    }


    public static function chooseDateRange($time)
    {
        if ($time == 'weekly')

            return Helperss::detectWeek(Carbon::now('+2')->format('l'));

        else if ($time=='monthly')

            return Helperss::detectMonth();

        else if ($time=='summary')
            return Helperss::detectSummary();
    }


    public static function loadReportsView($query = "", $projects = null, $users = null, $workspaces = null, $groupedBy = null, $sortedBy = null, $time = null)
    {
        $numeration = 0;

        $date_array = Helperss::chooseDateRange($time);


        switch ($query) {

            case "project":
                {

                    foreach ($users as $user) {

                        foreach ($projects as $project) {


                            $result_array[$numeration++] = DB::table('time_entries')
                                ->join('users', 'fk_for_user', '=', 'user_id')
                                ->join('projects', 'fk_for_projects', '=', 'project_id')
                                ->where('fk_for_user', $user->user_id)
                                ->where('fk_for_projects', $project)
                                ->whereBetween('start_time', [$date_array['start'], $date_array['end']])
                                ->select(

                                    'projects.project_name',
                                    'users.user_fname',
                                    'users.user_lname',
                                    'time_entries.duration',
                                    'time_entries.description',
                                    'time_entries.start_time'

                                )
                                ->get();
                        }
                    }


                    return $result_array;
                    break;
                }
            case"user":
                {

                    foreach ($users as $user) {

                        foreach ($projects as $project) {


                            $result_array[$numeration++] = DB::table('time_entries')
                                ->join('users', 'fk_for_user', '=', 'user_id')
                                ->join('projects', 'fk_for_projects', '=', 'project_id')
                                ->where('fk_for_user', $user)
                                ->where('fk_for_projects', $project->project_id)
                                ->whereBetween('start_time', [$date_array['start'], $date_array['end']])
                                ->select(

                                    'projects.project_name',
                                    'users.user_fname',
                                    'users.user_lname',
                                    'time_entries.duration',
                                    'time_entries.description',
                                    'time_entries.start_time'

                                )
                                ->get();
                        }
                    }

                    return $result_array;
                    break;
                }
            case "workspace":
                {

                    foreach ($workspaces as $workspace) {
                        foreach ($users as $user) {

                            foreach ($projects as $project) {


                                $result_array[$numeration++] = DB::table('time_entries')
                                    ->join('users', 'fk_for_user', '=', 'user_id')
                                    ->join('projects', 'fk_for_projects', '=', 'project_id')
                                    ->where('fk_for_user', $user->user_id)
                                    ->where('fk_for_projects', $project->project_id)
                                    ->where('time_entries.workspace_id', $workspace)
                                    ->whereBetween('start_time', [$date_array['start'], $date_array['end']])
                                    ->select(

                                        'projects.project_name',
                                        'users.user_fname',
                                        'users.user_lname',
                                        'time_entries.duration',
                                        'time_entries.description',
                                        'time_entries.start_time'

                                    )
                                    ->get();
                            }
                        }

                    }
                    return $result_array;
                    break;
                }
            case"project user":
                {

                    foreach ($users as $user) {

                        foreach ($projects as $project) {


                            $result_array[$numeration++] = DB::table('time_entries')
                                ->join('users', 'fk_for_user', '=', 'user_id')
                                ->join('projects', 'fk_for_projects', '=', 'project_id')
                                ->where('fk_for_user', $user)
                                ->where('fk_for_projects', $project)
                                ->where('time_entries.workspace_id', $workspaces)
                                ->whereBetween('start_time', [$date_array['start'], $date_array['end']])
                                ->select(

                                    'projects.project_name',
                                    'users.user_fname',
                                    'users.user_lname',
                                    'time_entries.duration',
                                    'time_entries.description',
                                    'time_entries.start_time'

                                )
                                ->get();
                        }
                    }

                    return $result_array;
                    break;
                }

            case"project workspace":
                {

                    {
                        foreach ($workspaces as $workspace) {
                            foreach ($users as $user) {

                                foreach ($projects as $project) {


                                    $result_array[$numeration++] = DB::table('time_entries')
                                        ->join('users', 'fk_for_user', '=', 'user_id')
                                        ->join('projects', 'fk_for_projects', '=', 'project_id')
                                        ->where('fk_for_user', $user->user_id)
                                        ->where('fk_for_projects', $project)
                                        ->where('time_entries.workspace_id', $workspace)
                                        ->whereBetween('start_time', [$date_array['start'], $date_array['end']])
                                        ->select(

                                            'projects.project_name',
                                            'users.user_fname',
                                            'users.user_lname',
                                            'time_entries.duration',
                                            'time_entries.description',
                                            'time_entries.start_time'

                                        )
                                        ->get();
                                }
                            }

                        }

                    }
                    return $result_array;
                    break;
                }

            case"user workspace":
                {

                    {
                        foreach ($workspaces as $workspace) {
                            foreach ($users as $user) {

                                foreach ($projects as $project) {


                                    $result_array[$numeration++] = DB::table('time_entries')
                                        ->join('users', 'fk_for_user', '=', 'user_id')
                                        ->join('projects', 'fk_for_projects', '=', 'project_id')
                                        ->where('fk_for_user', $user)
                                        ->where('fk_for_projects', $project->project_id)
                                        ->where('time_entries.workspace_id', $workspace)
                                        ->whereBetween('start_time', [$date_array['start'], $date_array['end']])
                                        ->select(

                                            'projects.project_name',
                                            'users.user_fname',
                                            'users.user_lname',
                                            'time_entries.duration',
                                            'time_entries.description',
                                            'time_entries.start_time'

                                        )
                                        ->get();
                                }
                            }

                        }

                    }
                    return $result_array;
                    break;
                }
            case"project user workspace":
                {
                    {
                        foreach ($workspaces as $workspace) {
                            foreach ($users as $user) {

                                foreach ($projects as $project) {


                                    $result_array[$numeration++] = DB::table('time_entries')
                                        ->join('users', 'fk_for_user', '=', 'user_id')
                                        ->join('projects', 'fk_for_projects', '=', 'project_id')
                                        ->where('fk_for_user', $user)
                                        ->where('fk_for_projects', $project)
                                        ->where('time_entries.workspace_id', $workspace)
                                        ->whereBetween('start_time', [$date_array['start'], $date_array['end']])
                                        ->select(

                                            'projects.project_name',
                                            'users.user_fname',
                                            'users.user_lname',
                                            'time_entries.duration',
                                            'time_entries.description',
                                            'time_entries.start_time'

                                        )
                                        ->get();
                                }
                            }

                        }
                    }
                    return $result_array;
                    break;
                }

            default:
                {

                    echo 'error';

                }

        }

    }

    /*groupByProjects
        groupByUsers
        groupByWorkspace

        user
        timeEntry
        project
        */

    public static function groupAndSortBy($result_array, $group_array, $sort_array)
    {

        $numeration = 0;
        $project_name = null;
        $sum = 0;
        $time_array=null;



        if ($group_array['group-by'] == 'groupByProjects' and $group_array['sub-group-by'] == 'user') {
            foreach ($result_array as $array_of_entries) {

                foreach ($array_of_entries as $entries) {

                    $time_array[$entries->project_name][$numeration++] = $entries->user_fname . ' ' . $entries->user_lname;

                    if ($project_name == $entries->project_name) {
                        $sum += $entries->duration;

                    } else if ($project_name == null)
                        $sum += $entries->duration;

                    $sum_array[$entries->project_name] = $sum;
                    $project_name = $entries->project_name;

                }
                $project_name = null;
                $sum = 0;
            }

        } else if ($group_array['group-by'] == 'groupByUsers' and $group_array['sub-group-by'] == 'project') {
            foreach ($result_array as $array_of_entries) {

                foreach ($array_of_entries as $entries) {

                    $time_array[$entries->user_fname . ' ' . $entries->user_lname][$numeration++] = $entries->project_name;

                    if ($project_name == $entries->project_name) {
                        $sum += $entries->duration;

                    } else if ($project_name == null)
                        $sum += $entries->duration;

                    $sum_array[$entries->project_name] = $sum;
                    $project_name = $entries->project_name;
                }
                $project_name = null;
                $sum = 0;
            }

        } else if ($group_array['group-by'] == 'groupByProjects' and $group_array['sub-group-by'] == 'timeEntry') {
            foreach ($result_array as $array_of_entries) {

                foreach ($array_of_entries as $entries) {

                    $time_array[$entries->project_name][$numeration++] = $entries->description;

                    if ($project_name == $entries->project_name) {
                        $sum += $entries->duration;

                    } else if ($project_name == null)
                        $sum += $entries->duration;

                    $sum_array[$entries->project_name] = $sum;
                    $project_name = $entries->project_name;
                }
                $project_name = null;
                $sum = 0;
            }

        } else if ($group_array['group-by'] == 'groupByProjects' and $group_array['sub-group-by'] == 'timeEntry') {
            foreach ($result_array as $array_of_entries) {

                foreach ($array_of_entries as $entries) {

                    $time_array[$entries->project_name][$numeration++] = $entries->description;

                    /*if ($project_name == $entries->project_name) {
                        $sum += $entries->duration;

                    } else if ($project_name == null)
                        $sum += $entries->duration;

                    $sum_array[$entries->project_name] = $sum;
                    $project_name = $entries->project_name;
                }
                $project_name = null;
                $sum = 0;*/

                }

            }
        } else if ($group_array['group-by'] == 'groupByWorkspace' and $group_array['sub-group-by'] == 'project') {
            foreach ($result_array as $array_of_entries) {

                foreach ($array_of_entries as $entries) {

                    $time_array[$entries->user_fname . ' ' . $entries->user_lname][$numeration++] = $entries->project_name;

                    if ($project_name == $entries->project_name) {
                        $sum += $entries->duration;

                    } else if ($project_name == null)
                        $sum += $entries->duration;

                    $sum_array[$entries->project_name] = $sum;
                    $project_name = $entries->project_name;
                }
                $project_name = null;
                $sum = 0;
            }

        } else if ($group_array['group-by'] == 'groupByWorkspace' and $group_array['sub-group-by'] == 'user') {
            foreach ($result_array as $array_of_entries) {

                foreach ($array_of_entries as $entries) {

                    $time_array[$entries->user_fname . ' ' . $entries->user_lname][$numeration++] = $entries->description;

                    /* if ($project_name == $entries->project_name) {
                         $sum += $entries->duration;

                     } else if ($project_name == null)
                         $sum += $entries->duration;

                     $sum_array[$entries->project_name] = $sum;
                     $project_name = $entries->project_name;
                 }
                 $project_name = null;
                 $sum = 0;*/
                }

            }
        } else if ($group_array['group-by'] == 'groupByWorkspace' and $group_array['sub-group-by'] == 'timeEntry') {
            foreach ($result_array as $array_of_entries) {

                foreach ($array_of_entries as $entries) {

                    $time_array[$entries->project_name][$numeration]['value'] = $entries->user_fname . ' ' . $entries->user_lname;

                    if ($project_name == $entries->project_name) {
                        $sum += $entries->duration;

                    } else if ($project_name == null)
                        $sum += $entries->duration;

                    $time_array[$entries->project_name][$numeration++]['sum'] = $sum;
                    $project_name = $entries->project_name;
                }
                $project_name = null;
                $sum = 0;
            }

        }
var_dump($time_array);

die();
//        ksort($time_array);
        /*var_dump($time_array);

        die();*/

        /*if ($sort_array['sort-by'] == 'title') {

            if ($sort_array['sort-dir'] == 'asc')
                ksort($time_array);
            else krsort($time_array);
        } else {
            foreach ($time_array as $array) {
                if ($sort_array['sort-dir'] == 'asc') {

                    asort($array);

                }

                else {
                    arsort($array);
                    var_dump($array);

                }
            }

        }*/


    }

    /*public static function sortBySum($sortArray){
        $array=[];

        foreach ($sortArray as $item)
            $array=$item;

        foreach($array as $sumCheck)





    }*/

}







