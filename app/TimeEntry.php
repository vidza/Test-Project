<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TimeEntry extends Model
{
    protected $primaryKey="time_entry_id";
    protected $table="time_entries";

    public function userEntity(){

        return $this->belongsTo('App\User','user_id','fk_for_user');

    }

    public function updateEntity(){

        return $this->belongsTo('App\Update','update_id','fk_for_update');

    }
    public function projectEntity(){

        return $this->belongsTo('App\Project','project_id','fk_for_projects');

    }

}
