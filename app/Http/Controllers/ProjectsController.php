<?php

namespace App\Http\Controllers;

use App\TogglProject;
use App\Helperss\Helperss;
use App\Workspace;
use Request;
use Ixudra\Toggl\Facades\Toggl;
use Ixudra\Toggl\TogglService;
use Ixudra\Curl\Facades\Curl;
use App\Project;
use Validator;
use DB;

class ProjectsController extends AuthController
{
    public function index()
    {

        $project_model = Project::all();
        $projects = Project::all()->toArray();

        $i = 0;
        foreach ($project_model as $project) {

            if (!empty($project->togglProjectEntity()->first()->toggl_project_id)) {
                $projects[$i]['toggl_id'] = $project->togglProjectEntity()->first()->toggl_project_id;
                $projects[$i]['workspace_name'] = DB::table('workspace')
                    ->select('workspace_name')
                    ->where('workspace_id', $project->togglProjectEntity()->first()->workspace_id)->first()->workspace_name;
                $i++;
            } else $projects[$i++]['toggl_id'] = "";
        }


        return view('projects.projects', compact('projects'));

    }

    public function sync()
    {

        $workspaces = Workspace::all();

        foreach ($workspaces as $workspace) {

            $togglService = new TogglService($workspace->workspace_id, $workspace->toggl_token);


            $responses = $togglService->projects_w($workspace->workspace_id);
            if ($responses != [])
                foreach ($responses as $item) {

                    if (Project::where('project_name', $item->name)->count() == 0) {
                        DB::table('projects')->insert([

                            'project_name' => $item->name

                        ]);
                    }
                    $object = DB::table('projects')->where('project_name', $item->name)->first();


                    if (TogglProject::where('toggl_project_id', $item->id)->count() == 0) {

                        DB::table('toggl_projects')->insert([

                            'toggl_project_id' => $item->id,
                            'workspace_id' => $item->wid,
                            'fk_for_project' => $object->project_id,
                            'fk_for_workspace' => $workspace->w_id
                        ]);

                    }

                }
        }
        return redirect()->action('ProjectsController@index');
    }


    public function add()
    {

        $numeration = 0;
        $toggl_project = [];

        $repositories = [['name' => 'Toggl'], ['name' => 'Gitlab']];

        $workspaces = Workspace::all();

        $response = Curl::to('https://gitlab.com/api/v4/groups')
            //    ->withHeader('Private-Token: d7j9soN_FEYvya4rsABL') //gdev
            ->withHeader('Private-Token: j8W1snLx6qRWGd-jzJz3')
            ->asJson()
            ->get();

        foreach ($response as $item) {

            $git_workspace[$numeration]['id'] = $item->id;
            $git_workspace[$numeration]['name'] = $item->name;
            $numeration++;
        }
        $numeration = 0;
        foreach ($workspaces as $workspace) {

            $togglService = new TogglService($workspace->workspace_id, $workspace->toggl_token);

            $togglProjects = $togglService->projects_w($workspace->workspace_id);

            foreach ($togglProjects as $togglProject) {
                if (!Helperss::in_array_field($togglProject->id, 'project_id', $toggl_project, true)) {

                    if (TogglProject::where('toggl_project_id', $togglProject->id)->count() == 0) {

                        $toggl_project[$numeration]['project_id'] = $togglProject->id;
                        $toggl_project[$numeration]['project_name'] = $togglProject->name;
                        $toggl_project[$numeration]['workspace_id'] = $togglProject->wid;
                        $toggl_project[$numeration]['workspace_name'] = DB::table('workspace')
                            ->where('workspace_id', $togglProject->wid)
                            ->first()->workspace_name;
                        $numeration++;
                    }
                }
            }
        }

        return view('projects.add', compact('repositories', 'workspaces', 'git_workspace', 'toggl_project'));
    }


    public function create()
    {

        $request = Request::all();

        $rules = [

            'project_name' => 'required|min:2'

        ];

        $validation = Validator::make($request, $rules);

        if ($validation->fails()) {

            return redirect()->action('ProjectsController@add')->withErrors($validation->errors())->withInput();
        }

        if (!empty($request['project_name'])) {

            $existing_project = DB::table('projects')->where('project_name', $request['project_name'])->count();

            if ($existing_project == 0) {
                /** ako ne postoji projekat */

                DB::table('projects')->insert([

                    'project_name' => $request['project_name'],

                ]);

                if (!empty($request['repository'])) {

                    foreach ($request['repository'] as $repos) {
                        if ($repos == 'Toggl') {

                            $object = DB::table('projects')->where('project_name', $request['project_name'])->first();

                            if ($request['togglProject'] != 0) {
                                preg_match_all('/[^\s]+/', $request['togglProject'], $match);
                                //  $match[0][0]-> project_id
                                //$match[0][1]-> workspace_id

                                $workspace_object = DB::table('workspace')
                                    ->where('workspace_id', $match[0][1])
                                    ->first();

                                DB::table('toggl_projects')->insert([

                                    'toggl_project_id' => $match[0][0],
                                    'workspace_id' => $match[0][1],
                                    'fk_for_project' => $object->project_id,
                                    'fk_for_workspace' => $workspace_object->w_id
                                ]);

                            }
                            if (!empty($request['workspace'])) {

                                if (count($request['workspace']) > 1) {

                                    foreach ($request['workspace'] as $workspace) {

                                        $workspace_object = DB::table('workspace')
                                            ->where('workspace_name', $workspace)
                                            ->first();
//proveri ovo
                                        $togglService = new TogglService($workspace_object->workspace_id, $workspace_object->toggl_token);
                                        $create_request = $togglService->createProject(['name' => $request['project_name']]);

                                        if (!empty($create_request)) {

                                            DB::table('toggl_projects')->insert([

                                                'toggl_project_id' => $create_request->data->id,
                                                'workspace_id' => $create_request->data->wid,
                                                'fk_for_project' => $object->project_id,
                                                'fk_for_workspace' => $workspace_object->w_id
                                            ]);
                                        }
                                    }
                                } else {

                                    $workspace_object = DB::table('workspace')
                                        ->where('workspace_name', $request['workspace'])
                                        ->first();

                                    $togglService = new TogglService($workspace_object->workspace_id, $workspace_object->toggl_token);
                                    $create_request = $togglService->createProject(['name' => $request['project_name']]);

                                    if (!empty($create_request)) {

                                        DB::table('toggl_projects')->insert([

                                            'toggl_project_id' => $create_request->data->id,
                                            'workspace_id' => $create_request->data->wid,
                                            'fk_for_project' => $object->project_id,
                                            'fk_for_workspace' => $workspace_object->w_id
                                        ]);

                                    }
                                }
                            }
                            /** ovde bi mogli da stavimo da odredjeni workspace mora da bude selektovan za toggl */
                        } else echo 'invalid request';

                        /** ovde se stavlja da projekat ili vec postoji ili je invalid request */
                    }

                    if ($repos == 'Gitlab') {

                        $git = Curl::to('https://gitlab.com/api/v4/projects')
                            ->withHeader('Private-Token: j8W1snLx6qRWGd-jzJz3')
                            ->withData(array('owned' => 'true'))
                            ->withData(array('name' => $request['project_name']))
                            ->asJson()
                            ->post();


                        DB::table('projects')->where('project_name', $request['project_name'])->update([
                            'git_project_id' => $git->id
                        ]);
                        if ($request['gitWorkspaceId'] != 0) {
                            $response = Curl::to('https://gitlab.com/api/v4/groups/' . $request['gitWorkspaceId'] . '/projects/' . $git->id)
                                ->withHeader('Private-Token: j8W1snLx6qRWGd-jzJz3')
                                ->withData(array('owned' => 'true'))
                                ->asJson()
                                ->post();

                        }
                    }
                }
            }
            /** ovde kazemo da projekat vec postoji */
        }
        /** ovde postavimo poruku da treba da se unese barem ime */

        return redirect()->action('ProjectsController@add');

    }


    public function edit($id)
    {

        $selected_project = Project::find($id);
        $workspaces = Workspace::all();

        $empty_git = true;
        $empty_toggl = true;

        $i = 0;
        $free_workspace = [];


        if (empty($selected_project->git_project_id)) {
            $all_git_projects = Curl::to('https://gitlab.com/api/v4/projects')
                ->withHeader('Private-Token: d7j9soN_FEYvya4rsABL')
                ->withData(array('owned' => 'true', 'per_page' => '100', 'page' => '1'))
                ->asJson()
                ->get();

            foreach ($all_git_projects as $git_project) {

                if (Project::where('git_project_id', $git_project->id)->count() == 0) {

                    $git_project_array[$i]['git_project_id'] = $git_project->id;
                    $git_project_array[$i]['project_name'] = $git_project->name;

                    $i++;

                }
            }
        }

        if (!empty($selected_project->git_project_id))
            $empty_git = false;

        if (!empty($selected_project->togglProjectEntity()->first()->toggl_project_id)) {
            $empty_toggl = false;
            $toggl_project = $selected_project->togglProjectEntity()->first();

        } else if (TogglProject::where('fk_for_project', $id)->count() == 0)

            $toggl_project = new TogglProject();


        $numeration = 0;
        $togglP = [];

        foreach ($workspaces as $workspace) {

            if (!empty($selected_project->togglProjectEntity()->first()->workspace_id)) {

                if ($selected_project->togglProjectEntity()->first()->workspace_id != $workspace->workspace_id) {

                    $free_workspace[$i]['workspace_name'] = $workspace->workspace_name;
                    $free_workspace[$i]['workspace_id'] = $workspace->workspace_id;
                    $i++;
                }
            } else {

                $free_workspace[$i]['workspace_name'] = $workspace->workspace_name;
                $free_workspace[$i]['workspace_id'] = $workspace->workspace_id;
                $i++;
            }

            $togglService = new TogglService($workspace->workspace_id, $workspace->toggl_token);

            $togglProjects = $togglService->projects_w($workspace->workspace_id);

            foreach ($togglProjects as $togglProject) {

                if (!Helperss::in_array_field($togglProject->id, 'project_id', $togglP, true)) {

                    if (TogglProject::where('toggl_project_id', $togglProject->id)->count() == 0) {

                        $togglP[$numeration]['project_id'] = $togglProject->id;
                        $togglP[$numeration]['project_name'] = $togglProject->name;
                        $togglP[$numeration]['workspace_id'] = $togglProject->wid;
                        $togglP[$numeration]['workspace_name'] = DB::table('workspace')
                            ->where('workspace_id', $togglProject->wid)
                            ->first()->workspace_name;
                        $numeration++;
                    }
                }
            }

        }


        $booleans['empty_git'] = $empty_git;
        $booleans['empty_toggl'] = $empty_toggl;

        return view('projects.edit', compact('selected_project', 'toggl_project', 'booleans', 'free_workspace', 'git_project_array', 'togglP'));

    }

    public function store()
    {
        $request = Request::all();;

        $rules = [

            'project_name' => 'required|min:2'

        ];

        $validation = Validator::make($request, $rules);

        if ($validation->fails()) {

            return redirect()->action('ProjectsController@edit', ['id' => $request['project_id']])->withErrors($validation->errors())->withInput();
        }

        $selected_project = Project::find($request['project_id']);


        if (!empty($request)) {

            if ($selected_project->project_name != $request['project_name']) {


                $selected_project->update([

                    "project_name" => $request['project_name']

                ]);
            }

            if (array_key_exists('toggl_project_id', $request)) {


                if (!empty($request['workspace'])) {

                    if (count($request['workspace']) > 1) {

                        foreach ($request['workspace'] as $workspace) {

                            $workspace_object = DB::table('workspace')
                                ->where('workspace_name', $workspace)
                                ->first();

                            $togglService = new TogglService($workspace_object->workspace_id, $workspace_object->toggl_token);
                            $create_request = $togglService->createProject(['name' => $request['project_name']]);

                            if (!empty($create_request)) {

                                DB::table('toggl_projects')->insert([

                                    'toggl_project_id' => $create_request->data->id,
                                    'workspace_id' => $create_request->data->wid,
                                    'fk_for_project' => $selected_project->project_id,
                                    'fk_for_workspace' => $workspace_object->w_id

                                ]);

                            }
                        }
                    } else {

                        $workspace_object = DB::table('workspace')
                            ->where('workspace_name', $request['workspace'])
                            ->first();

                        $togglService = new TogglService($workspace_object->workspace_id, $workspace_object->toggl_token);
                        $create_request = $togglService->createProject(['name' => $request['project_name']]);

                        if (!empty($create_request)) {
                            DB::table('toggl_projects')->insert([

                                'toggl_project_id' => $create_request->data->id,
                                'workspace_id' => $create_request->data->wid,
                                'fk_for_project' => $selected_project->project_id,
                                'fk_for_workspace' => $workspace_object->w_id

                            ]);
                        }
                    }

                }
                /** ili ce iz defaultnog workpsace-a da napravi ili mozemo setovati neki uslov */
            } else if (!empty($request['togglProject'])) {
                if ($request['togglProject'] != 0) {
                    preg_match_all('/[^\s]+/', $request['togglProject'], $match);
                    //  $match[0][0]-> project_id
                    //$match[0][1]-> workspace_id

                    $workspace_object = DB::table('workspace')
                        ->where('workspace_id', $match[0][1])
                        ->first();

                    DB::table('toggl_projects')->insert([

                        'toggl_project_id' => $match[0][0],
                        'workspace_id' => $match[0][1],
                        'fk_for_project' => $request['project_id'],
                        'fk_for_workspace' => $workspace_object->w_id
                    ]);
                }
            }


            if (array_key_exists('sync_git_project_id', $request)) {

                if ($request['sync_git_project_id'] != 0) {
                    DB::table('projects')
                        ->where('project_name', $request['project_name'])
                        ->update(['git_project_id' => $request['sync_git_project_id']]);


                } else if (array_key_exists("git_project_id", $request)) {

                    $git = Curl::to('https://gitlab.com/api/v4/projects')
                        ->withHeader('Private-Token: j8W1snLx6qRWGd-jzJz3')
                        ->withData(array('owned' => 'true'))
                        ->withData(array('name' => $request['project_name']))
                        ->asJson()
                        ->post();

                    DB::table('projects')
                        ->where('project_name', $request['project_name'])
                        ->update(['git_project_id' => $git->id]);

                }
            }
        }
        /** napisi poruku nijedna promena nije izvrsena */
        return redirect()->action('ProjectsController@edit', ['id' => $request['project_id']]);
    }


    public function delete()
    {

        $request = Request::all();

        $project = Project::find($request['project_id']);


        if (array_key_exists('all', $request)) {

            $toggl_project = $project->togglProjectEntity()->first();
            if (!empty($toggl_project->toggl_project_id)) {

                Toggl::deleteProject($toggl_project->toggl_project_id);
            }

            if (!empty($project['git_project_id'])) {

                $git = Curl::to('https://gitlab.com/api/v4/projects/' . $project['git_project_id'])
                    ->withHeader('Private-Token: j8W1snLx6qRWGd-jzJz3')
                    ->withData(array('owned' => 'true'))
                    ->asJson()
                    ->get();


                if (!empty($git->id)) {

                    Curl::to('https://gitlab.com/api/v4/projects/' . $project['git_project_id'])
                        ->withHeader('Private-Token: j8W1snLx6qRWGd-jzJz3')
                        ->withData(array('owned' => 'true'))
                        ->asJson()
                        ->delete();
                }
            }
            $project->delete();


        } else if (array_key_exists('giggl', $request))

            $project->delete();


        return redirect()->action('ProjectsController@index');
    }

}
