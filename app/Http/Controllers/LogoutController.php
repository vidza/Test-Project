<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LogoutController extends AuthController
{
    public function logout(){

        Auth::logout();

        Session::flash('message','Goodbye');

        return redirect('login');
}
}
