<?php

namespace App\Http\Controllers;

use App\Helperss\Helperss;
use App\Project;
use App\TimeEntry;
use App\Workspace;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Ixudra\Toggl\TogglService;
use Request;
use Carbon\Carbon;
use App\User;
use App\Update;
use Ixudra\Curl\Facades\Curl;

use Illuminate\Support\Facades\Auth;


class ReportsController extends AuthController
{
    public function index()
    {

        $user_array = User::all();
        $project_array = Project::all();
        $update_array = Update::all()->last();


        return view('reports.reports', compact('user_array', 'update_array', 'project_array'));

    }

    public function issuePage()
    {


        return view('reports.gitIssue');

    }


    public
    function store()
    {
        /** update all entries into database */

        $start_date = Update::all()->last()->time_of_update_atom;
        $end_date = Carbon::now('+2')->toAtomString();

        DB::table('updates')->insert([

            'time_of_update' => Carbon::now('+2')->addHour(),
            'time_of_update_atom' => Carbon::now('+2')->toAtomString()

        ]);

        $update = DB::table('updates')->orderBy('update_id', 'desc')->first();

        $togglApis = User::all();

        foreach ($togglApis as $togglApi) {

            $workspace = Workspace::find($togglApi->fk_for_workspace);

            $togglService = new TogglService($workspace->workspace_id, $togglApi->toggl_api);
            $response = $togglService->timeEntries($start_date, $end_date);

            if (!empty($response)) {
                foreach ($response as $item) {


                    $date1 = \DateTime::createFromFormat('Y-m-d\TH:i:sT', $item->start, '+2');
                    $date = $date1->format('Y-m-d H:i:s');

                    if (property_exists($item, 'pid')) {


                        $projects = DB::table('projects')
                            ->join('toggl_projects', 'project_id', '=', 'fk_for_project')
                            ->select('project_id', 'project_name')
                            ->where('toggl_project_id', $item->pid)
                            ->get();

                        if (property_exists($item, 'description')) {

                            $description = $item->description;

                        } else $description = "";

                        foreach ($projects as $project) {

                            $user = DB::table('users')->where('toggl_id', $item->uid)->first();

                            DB::table('time_entries')->insert([

                                'start_time' => $date,
                                'duration' => $item->duration,
                                'description' => $description,
                                'status_db' => true,
                                'fk_for_update' => $update->update_id,
                                'fk_for_user' => $user->user_id,
                                'fk_for_projects' => $project->project_id,
                                'single_entry_id' => $item->id,
                                'workspace_id' => $item->wid

                            ]);
                        }
                    }
                }
            } /** stavimo neku promenljivu da prikaze da nije prihvacen request, moze i update da se izbrise*/ else var_dump($response);
        }
        return redirect()->action('ReportsController@index');


    }

    public
    function selectedEntries()
    {

        $request = Request::all();
        $num = 0;
        $sum = 0;
        $project_name = null;
        $sum_array = null;
        $show_all_entries = false;


        $status = null;

        if (array_key_exists("project", $request)) {

            $status = 'project';

        }
        if (array_key_exists("user", $request)) {
            if (!empty($status))
                $status .= ' user';
            else
                $status = 'user';
        }

        if ($status == 'project user')

            $time_entries_array = Helperss::startEndDate($status, $request['start_date'], $request['end_date'], $request['project'], $request['user']);

        else if ($status == 'project')
            $time_entries_array = Helperss::startEndDate($status, $request['start_date'], $request['end_date'], $request['project']);

        else if ($status == 'user')
            $time_entries_array = Helperss::startEndDate($status, $request['start_date'], $request['end_date'], $request['user']);

        if (empty($time_entries_array)) {

            return redirect()->action('ReportsController@index');
        }

        foreach ($time_entries_array as $array_of_entries) {

            foreach ($array_of_entries as $entries) {

                $time_array[$entries->project_name][$entries->user_fname . ' ' . $entries->user_lname][$num++] = $entries;

                if ($project_name == $entries->project_name) {
                    $sum += $entries->duration;
                    $sum_array[$entries->project_name] = $sum;
                } else if ($project_name == null)
                    $sum += $entries->duration;

                else {
                    $sum = 0;
                }

                $project_name = $entries->project_name;
            }
        }


        return view('reports.show', compact('time_array', 'sum_array', 'show_all_entries'));


    }


    public
    function updateGit()
    {

        $request = Request::all();


        $issue_value_and_tag = null;
        $numeration = 0;
        $matches = null;


        if (!empty($request['add_issue'])) {

            foreach ($request['add_issue'] as $issue) {

                $time_entry = TimeEntry::find($issue);

                if (array_key_exists('tag', $request)) {


                    foreach ($request['tag'] as $tag) {

                        preg_match_all('/[^\s]+/', $tag, $match);

                        $issue_value_and_tag[$numeration]['issue_id'] = $match[0][0];
                        $issue_value_and_tag[$numeration++] ['tag'] = $match[0][1];

                    }

                    foreach ($issue_value_and_tag as $item) {

                        if ($item['issue_id'] == $time_entry->time_entry_id) {

                            $prepare_array['match'] = substr($item['tag'], 1, strlen($item['tag']) - 1);

                        }
                    }
                } else {

                    preg_match('/(\#[0-9]+\b)(?!;)/', $time_entry->description, $matches);
                    $prepare_array['match'] = substr($matches[0], 1, strlen($matches[0]) - 1);
                }


                $prepare_array['project_id'] = Project::find($time_entry->fk_for_projects)->git_project_id;
                $prepare_array['duration'] = $time_entry->duration;

                Curl::to('https://gitlab.com/api/v4/projects/' . $prepare_array['project_id'] . '/issues/' . $prepare_array['match'] . '/add_spent_time')
                    ->withHeader('Private-Token: j8W1snLx6qRWGd-jzJz3')//test- repository

//                    ->withHeader('Private-Token: d7j9soN_FEYvya4rsABL') //gdev
                    ->withData(array('duration' => $prepare_array['duration'] . 's'))
                    ->asJson()
                    ->post();
                TimeEntry::where('time_entry_id', $issue)->update([


                    'status_git' => true

                ]);
            }
        }

        return redirect()->action('ReportsController@index');
    }

    public
    function updateAllOnGit()
    {

        $start_date = Update::all()->first()->time_of_update_atom;
        $end_date = Carbon::now('+2')->toAtomString();


        $userTogglApi = User::find(Auth::user()->getAuthIdentifier());

        $workspace = Workspace::find($userTogglApi->fk_for_workspace);

        $togglService = new TogglService($workspace->workspace_id, $userTogglApi->toggl_api);
        $response = $togglService->timeEntries($start_date, $end_date);

        if (!empty($response)) {
            foreach ($response as $item) {

                if ($item->duration == 1) {

                    $togglService->deleteTimeEntry($item->id);

                } else if (TimeEntry::where('single_entry_id', $item->id)->count() == 0) {
                    $date1 = \DateTime::createFromFormat('Y-m-d\TH:i:sT', $item->start);
                    $date = $date1->format('Y-m-d H:i:s');

                    if (property_exists($item, 'pid')) {


                        $projects = DB::table('projects')
                            ->join('toggl_projects', 'project_id', '=', 'fk_for_project')
                            ->select('project_id', 'project_name')
                            ->where('toggl_project_id', $item->pid)
                            ->get();

                        if (property_exists($item, 'description')) {

                            $description = $item->description;

                        } else $description = "";

                        foreach ($projects as $project) {

                            $user = DB::table('users')->where('toggl_id', $item->uid)->first();


                            DB::table('time_entries')->insert([

                                'start_time' => $date,
                                'duration' => $item->duration,
                                'description' => $description,
                                'status_db' => true,
                                'fk_for_update' => null,
                                'fk_for_user' => $user->user_id,
                                'fk_for_projects' => $project->project_id,
                                'single_entry_id' => $item->id,
                                'workspace_id' => $item->wid

                            ]);
                        }
                    }
                }
            }
        }

        $user_entries = TimeEntry::where('fk_for_user', Auth::user()->getAuthIdentifier())->get();

        $matches = null;

        foreach ($user_entries as $entry) {

            if ($entry->status_git == false) {
                if ((preg_match('/(\#[0-9]+\b)(?!;)/', $entry->description, $matches) == 1)) {
                    $prepare_array['match'] = substr($matches[0], 1, strlen($matches[0]) - 1);


                    $prepare_array['project_id'] = Project::find($entry->fk_for_projects)->git_project_id;
                    $prepare_array['duration'] = $entry->duration;

                    Curl::to('https://gitlab.com/api/v4/projects/' . $prepare_array['project_id'] . '/issues/' . $prepare_array['match'] . '/add_spent_time')
                        ->withHeader('Private-Token: '.User::where('user_id',Auth::user()->getAuthIdentifier())->first()->git_key)//test- repository

                  //->withHeader('Private-Token: d7j9soN_FEYvya4rsABL') //gdev
                        ->withData(array('duration' => $prepare_array['duration'] . 's'))
                        ->asJson()
                        ->post();

                    $entry->status_git = true;
                    $entry->save();
                }
            }
        }
        return redirect()->action('HomeController@index');
    }


    public
    function showAll()
    {

        $i = 0;
        $num = 0;
        $users = User::all();
        $projects = Project::all();
        $time_entries = TimeEntry::all();
        $workspaces = Workspace::all();
        $project_name = null;
        $sum = 0;
        $time_array = null;


        $current_time = Carbon::now('+2');

        $date_array = Helperss::detectWeek($current_time->format('l'));

        foreach ($users as $user) {

            foreach ($projects as $project) {


                $time_entries_array[$i++] = DB::table('time_entries')
                    ->join('users', 'fk_for_user', '=', 'user_id')
                    ->join('projects', 'fk_for_projects', '=', 'project_id')
                    ->where('fk_for_user', $user->user_id)
                    ->where('fk_for_projects', $project->project_id)
                    ->whereBetween('start_time', [$date_array['start'], $date_array['end']])
                    ->orderBy('projects.project_name')
                    ->select('users.user_fname',
                        'users.user_lname',
                        'users.user_id',
                        'projects.project_id',
                        'time_entries.time_entry_id',
                        'time_entries.start_time',
                        'time_entries.duration',
                        'time_entries.description',
                        'projects.project_name'
                    )
                    ->get();
            }
        }

        foreach ($time_entries_array as $array_of_entries) {

            foreach ($array_of_entries as $entries) {

                $time_array[$entries->project_name][$entries->user_fname . ' ' . $entries->user_lname][$num++] = $entries;

                if ($project_name == $entries->project_name) {
                    $sum += $entries->duration;

                } else if ($project_name == null)
                    $sum += $entries->duration;

                $sum_array[$entries->project_name] = $sum;
                $project_name = $entries->project_name;
            }
            $project_name = null;
            $sum = 0;
        }


        return view('reports.all', compact('users', 'projects', 'time_entries', 'workspaces', 'time_array', 'sum_array'));

    }

    public
    function partialFilter()
    {

        $num = 0;
        $users = User::all();
        $projects = Project::all();
        $workspaces = Workspace::all();
        $project_name = null;
        $sum = 0;
        $time_array = null;

        $request = Request::all();


        $sort_request['sort-dir'] = $request['sort-dir'];
        $sort_request['sort-by'] = $request['sort-by'];

        $groupedBy_request['group-by'] = $request['group-by'];
        $groupedBy_request['sub-group-by'] = $request['sub-group-by'];

        $time_request['time'] = $request['time'];

        $query = "";
        if (array_key_exists('project', $request)) {

            $query = "project ";
            $project_request = $request['project'];

        } else
            $project_request = $projects;


        if (array_key_exists('user', $request)) {

            $query .= "user ";
            $user_request = $request['user'];

        } else
            $user_request = $users;

        if (array_key_exists('workspace', $request)) {
            $query .= "workspace ";
            $workspace_request = $request['workspace'];
        } else
            $workspace_request = $workspaces;


        $result_array = Helperss::loadReportsView(substr($query, 0, strlen($query) - 1), $project_request, $user_request, $workspace_request, $groupedBy_request, $sort_request, $time_request);


        Helperss::groupAndSortBy($result_array, $groupedBy_request, $sort_request);


        return view('reports.tabs', compact('users', 'projects', 'time_entries', 'workspaces', 'time_array', 'sum_array'));


    }

    public
    function showEntries()
    {


        $request = Request::all();
        $num = 0;
        $sum = 0;
        $project_name = null;
        $sum_array = null;
        $show_all_entries = false;


        $status = null;

        if (array_key_exists("project", $request)) {

            $status = 'project';

        }
        if (array_key_exists("user", $request)) {
            if (!empty($status))
                $status .= ' user';
            else
                $status = 'user';
        }

        if ($status == 'project user')

            $time_entries_array = Helperss::startEndDate($status, $request['start_date'], $request['end_date'], $request['project'], $request['user']);

        else if ($status == 'project')
            $time_entries_array = Helperss::startEndDate($status, $request['start_date'], $request['end_date'], $request['project']);

        else if ($status == 'user')
            $time_entries_array = Helperss::startEndDate($status, $request['start_date'], $request['end_date'], $request['user']);

        if (empty($time_entries_array)) {

            return redirect()->action('ReportsController@index');
        }

        foreach ($time_entries_array as $array_of_entries) {

            foreach ($array_of_entries as $entries) {

                $time_array[$entries->project_name][$entries->user_fname . ' ' . $entries->user_lname][$num++] = $entries;

                if ($project_name == $entries->project_name) {
                    $sum += $entries->duration;
                    $sum_array[$entries->project_name] = $sum;
                } else if ($project_name == null)
                    $sum += $entries->duration;

                else {
                    $sum = 0;
                }

                $project_name = $entries->project_name;
            }
        }

        return view('reports.entriesview', compact('time_array', 'sum_array', 'show_all_entries'));

    }


}