<?php

namespace App\Http\Controllers;

use App\Client;
use App\User;
use App\Project;
use App\TogglProject;
use App\Workspace;
use Illuminate\Support\Facades\DB;
use Ixudra\Toggl\Facades\Toggl;
use Ixudra\Toggl\TogglService;
use Request;
use Carbon\Carbon;


class ReportsApiController extends Controller
{
    public function glCreateIssue()
    {

        $request = Request::all();

        $object_attributes = $request['object_attributes'];
        $project = $request['project'];

        $workspace_objects = Workspace::all();

        foreach ($workspace_objects as $workspace_object) {
            $togglService = new TogglService($workspace_object->workspace_id, $workspace_object->toggl_token);

            if ($object_attributes['action'] == 'open') {

                if (Project::where('git_project_id', $project['id'])->count() == 0) {

                    /** kreiramo projekat u bazi*/
                    DB::table('projects')->insert([

                        'project_name' => $project['name'],
                        'git_project_id' => $project['id']

                    ]);
                } else if (is_null(Project::where('git_project_id', $project['id'])->first()->git_project_id)) {
                    Project::where('git_project_id', $project['id'])->update([

                        'git_project_id' => $project['id']

                    ]);
                }

                $project_object = DB::table('projects')->where('git_project_id', $project['id'])->first();

                /** kreiramo projekat na Toggl-u*/


                //Provera da li klijent postoji u nasoj bazi ako ne pravi se
                //ovde je potrebno da imamo medjutabelu za clijente i workspacesve

                $project_title = explode('/', $project['path_with_namespace']);
                $client_name = str_replace('_', ' ', $project_title[1]);

                if (count($project_title) != 3) {

                    if (Client::where('client_name', $client_name)->count() == 0) {
                        Client::insert([

                            'client_name' => $client_name

                        ]);
                    }
                    $client_id = Client::where('client_name', $client_name)->first()->c_id;

                    $client_toggl_id = null;

                } else {
                    if (Client::where('client_name', $client_name)->count() == 0) {
                        $created_client = $togglService->createClient(['name' => $client_name]);

                        Client::insert([

                            'client_name' => $created_client->data->name

                        ]);

                        $client_id = Client::where('client_name', $created_client->data->name)->first()->c_id;
                        $client_toggl_id = $created_client->data->id;

                    } else {

                        $client_id = Client::where('client_name', $client_name)->first()->c_id;

                        $client_entities = DB::table('client_toggl')->where('client_id', $client_id)->get();

                        $notExistsOrDifferent = false;
                        $match = false;
                        foreach ($client_entities as $client_entity) {

                            if ($workspace_object->w_id != $client_entity->workspace_id)
                                $notExistsOrDifferent = true;

                            if ($workspace_object->w_id == $client_entity->workspace_id)
                                $match = true;
                        }
                        if ($notExistsOrDifferent) {

                            if ($match) {
                                $client_entity = DB::table('client_toggl')->where('client_id', $client_id)->where('workspace_id',$workspace_object->w_id)->first();
                                $client_toggl_id = $client_entity->toggl_client_id;

                            } else {

                                $created_client = $togglService->createClient(['name' => $client_name]);

                                $client_toggl_id = $created_client->data->id;

                            }
                        }
                    }
                }


                if (is_null(TogglProject::where('fk_for_project', $project_object->project_id)->first())) {

                    $create_request = $togglService->createProject(['name' => $project['name'], 'is_private' => false, 'cid' => $client_toggl_id]);

                    if (!empty($create_request)) {
                        /** kreiramo projekat u bazi za Toggl*/

                        TogglProject::insert([

                            'toggl_project_id' => $create_request->data->id,
                            'workspace_id' => $create_request->data->wid,
                            'fk_for_project' => $project_object->project_id,
                            'fk_for_workspace' => $workspace_object->w_id

                        ]);

                        $tp_id = TogglProject::where('toggl_project_id', $create_request->data->id)->first()->tp_id;


                        DB::table('client_toggl')->insert([

                            'client_id' => $client_id,
                            'toggl_id' => $tp_id,
                            'workspace_id' => $workspace_object->w_id,
                            'toggl_client_id' => $client_toggl_id,

                        ]);
                    }

                } else {

                    $notExistsOrDifferent = false;
                    $match = false;
                    $client_toggl_entities = DB::table('client_toggl')
                        ->join('toggl_projects', 'toggl_id', '=', 'tp_id')
                        ->where('fk_for_project', $project_object->project_id)->get();

                    foreach ($client_toggl_entities as $client_toggl_entity) {
                        if ($client_toggl_entity->workspace_id != $workspace_object->w_id)
                            $notExistsOrDifferent = true;

                        if ($client_toggl_entity->fk_for_workspace == $workspace_object->w_id)
                            $match = true;


                    }
                    if ($notExistsOrDifferent) {

                        if ($match) {

                            $tp_id = TogglProject::where('fk_for_project', $project_object->project_id)->where('fk_for_workspace', $workspace_object->w_id)->first()->tp_id;

                            if (DB::table('client_toggl')->where('toggl_id', $tp_id)->count() == 0) {

                                DB::table('client_toggl')->insert([

                                    'client_id' => $client_id,
                                    'toggl_id' => $tp_id,
                                    'workspace_id' => $workspace_object->w_id,
                                    'toggl_client_id' => $client_toggl_id,

                                ]);
                            }

                        } else {

                            $create_request = $togglService->createProject(['name' => $project['name'], 'cid' => $client_toggl_id, 'is_private' => false]);

                            if (!empty($create_request)) {
                                /** kreiramo projekat u bazi za Toggl*/

                                TogglProject::insert([

                                    'toggl_project_id' => $create_request->data->id,
                                    'workspace_id' => $create_request->data->wid,
                                    'fk_for_project' => $project_object->project_id,
                                    'fk_for_workspace' => $workspace_object->w_id

                                ]);

                                $tp_id = TogglProject::where('toggl_project_id', $create_request->data->id)->first()->tp_id;

                                DB::table('client_toggl')->insert([

                                    'client_id' => $client_id,
                                    'toggl_id' => $tp_id,
                                    'workspace_id' => $workspace_object->w_id,
                                    'toggl_client_id' => $client_toggl_id,

                                ]);
                            }
                        }
                    }
                }


                $toggl_project_id = TogglProject::where('fk_for_project', $project_object->project_id)->where('fk_for_workspace', $workspace_object->w_id)->first()->toggl_project_id;

                $i = 0;

                if (array_key_exists('assignees', $request)) {
                    foreach ($request['assignees'] as $assignee) {
                        if (!empty(User::where('git_username', $assignee['username'])->get())) {

                            $users[$i++] = User::where('git_username', $assignee['username'])->first();

                        }
                    }
                }
                if (!empty($users)) {

                    foreach ($users as $user) {

                        if (!empty($user->toggl_api)) {
                            $serviceToggl = new TogglService($workspace_object->workspace_id, $user->toggl_api);
                            $response = $serviceToggl->createTimeEntry(['description' => '#' . $object_attributes['iid'] . ' ' . $object_attributes['title'], 'duration' => '1', 'pid' => $toggl_project_id, 'start' => $time = Carbon::now('+2')->toIso8601String(), 'created_with' => 'curl']);
                        }
                    }
                }
            }

            else if ($object_attributes['action'] == 'update') {

                if (array_key_exists('assignees', $request['changes'])) {

                    $i = 0;

                    $project_object = DB::table('projects')->where('project_name', $project['name'])->first();

                    $toggl_project_id = TogglProject::where('fk_for_project', $project_object->project_id)->where('fk_for_workspace', $workspace_object->w_id)->first()->toggl_project_id;


                    foreach ($request['changes']['assignees']['current'] as $assignee) {

                        $exists=false;
                        if(!empty($request['changes']['assignees']['previous'])) {
                            foreach ($request['changes']['assignees']['previous'] as $previous_assignee) {

                                if ($previous_assignee['username'] == $assignee['username']) {

                                    $exists = true;
                                }
                            }
                        }

                        if (!$exists) {

                            if (!empty(User::where('git_username', $assignee['username'])->get())) {

                                $users[$i++] = User::where('git_username', $assignee['username'])->first();

                            }
                        }
                    }

                    if (!empty($users)) {

                        foreach ($users as $user) {

                            if (!empty($user->toggl_api)) {
                                $serviceToggl = new TogglService($workspace_object->workspace_id, $user->toggl_api);
                                $response = $serviceToggl->createTimeEntry(['description' => '#' . $object_attributes['iid'] . ' ' . $object_attributes['title'], 'duration' => '1', 'pid' => $toggl_project_id, 'start' => $time = Carbon::now('+2')->toIso8601String(), 'created_with' => 'curl']);
                            }
                        }
                    }
                }
            }
        }


        return response()->json(['status' => 'Webhook received successfully'], 200);

    }

}
