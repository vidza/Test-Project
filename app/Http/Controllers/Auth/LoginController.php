<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Request;
use Validator;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login()
    {
        $request = Request::all();

        $rules = [
            'user_email' => 'required|string|email',
            'password' => 'required|string|min:6'


        ];

        $validator = Validator::make($request, $rules);


        if ($validator->fails())
            return redirect()->to('login')->withErrors($validator->errors())->withInput();

        if (Auth::attempt(['user_email' => $request['user_email'], 'password' => $request['password']])) {

            return redirect()->intended();

        } else {
                $login_error="Username or password is incorrect";

            return view('auth.login', compact('login_error'));
        }
        }

}
