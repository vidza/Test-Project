<?php

namespace App\Http\Controllers;

use App\Workspace;
use Request;
use Validator;
use DB;


class WorkspaceController extends AuthController
{


    public function index(){

        $workspaces=Workspace::all();

        return view('workspace.workspace',compact('workspaces'));

    }


    public function add(){

        return view('workspace.add');

    }

    public function store(){

        $request=Request::all();

        $rules=[

            'workspace_name' => 'required',
            'workspace_id' => 'required|integer',
            'toggl_token' => 'required'

        ];

        $validation = Validator::make($request, $rules);
        if ($validation->fails()) {

            return redirect()->action('WorkspaceController@add')->withErrors($validation->errors())->withInput();

        }

        DB::table('workspace')->insert([

           'toggl_token' => $request['toggl_token'],
           'workspace_id' => $request['workspace_id'],
           'workspace_name' => $request['workspace_name']


        ]);


        return redirect()->action('WorkspaceController@add');
    }


    public function edit($id){

        $workspace=Workspace::find($id);

        return view('workspace.edit',compact('workspace'));

    }

    public function update(){

        $request=Request::all();


        $rules=[

            'workspace_name' => 'required',
            'workspace_id' => 'required|integer',
            'toggl_token' => 'required'

        ];

        $validation = Validator::make($request, $rules);
        if ($validation->fails()) {

            return redirect()->action('WorkspaceController@edit',$request['w_id'])->withErrors($validation->errors())->withInput();

        }

        DB::table('workspace')->where('w_id',$request['w_id'])->update([

            'toggl_token' => $request['toggl_token'],
            'workspace_id' => $request['workspace_id'],
            'workspace_name' => $request['workspace_name']


        ]);
        return redirect()->action('WorkspaceController@index');

    }

    public function delete(){


        $request=Request::all();

        Workspace::where('w_id',$request['workspace_id'])->delete();

        return redirect()->action('WorkspaceController@index');
    }

}
