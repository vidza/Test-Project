<?php

namespace App\Http\Controllers;


use App\Helperss\Helperss;
use App\Workspace;
use Ixudra\Toggl\TogglService;
use Request;
use App\User;
use App\Role;
use App\Permission;
use DB;
use Validator;
use Ixudra\Curl\Facades\Curl;


class UsersController extends AuthController
{

    public function index()
    {
        $numeration = 0;
        $i = 0;
        $user = User::all();

        foreach ($user as $user_workspace) {
            $workspace = Workspace::find($user_workspace->fk_for_workspace);
            $workspace_name[$i++] = $workspace->workspace_name;
        }


        return view('users.users', compact('user', 'numeration', 'workspace_name'));

    }


    public function edit($id)
    {
        $selected_user = User::find($id);
        $role = $selected_user->roles()->first()->id;
        $usernames = [];
        $i = 0;

        $git_usernames = Curl::to('https://gitlab.com/api/v4/groups/80635/members')
            ->withHeader('Private-Token: d7j9soN_FEYvya4rsABL')
            ->withData(array('owned' => 'true'))
            ->asJson()
            ->get();

        foreach ($git_usernames as $git_username) {
            if (!Helperss::in_array_field($git_username->id, 'git_id', $usernames, true)) {


                if (User::where('git_id', $git_username->id)->count() == 0) {

                    $usernames[$i]['id'] = $git_username->id;
                    $usernames[$i]['name'] = $git_username->username;
                    $i++;
                }
            }
        }

        $roles = Role::all();

        $numeration = 0;
        $workspace = Workspace::find($selected_user->fk_for_workspace);
        $workspace_model = Workspace::all();
        $workspaces = [];
        foreach ($workspace_model as $item) {
            if ($item->w_id != $workspace->w_id) {
                $workspaces[$numeration]['id'] = $item->w_id;
                $workspaces[$numeration]['name'] = $item->workspace_name;
                $numeration++;
            }
        }

        $toggl_users_array = [];
        $i = 0;


        $togglservice = new TogglService($workspace->workspace_id, $workspace->toggl_token);

        $variables = $togglservice->projectsUsers($workspace->workspace_id);

        foreach ($variables as $variable) {
            if (!Helperss::in_array_field($variable->id, 'toggl_id', $toggl_users_array, true)) {
                if (User::where('toggl_id', $variable->id)->count() == 0) {

                    $toggl_users_array[$i]['toggl_id'] = $variable->id;
                    $toggl_users_array[$i]['full_name'] = $variable->fullname;
                    $toggl_users_array[$i]['email'] = $variable->email;
                    $i++;
                }
            }
        }


        return view('users.edit', compact('selected_user', 'toggl_users_array', 'role', 'roles', 'workspace', 'workspaces', 'usernames'));

    }

    public function store()
    {


        $request = Request::all();

        $rules = [

            'user_fname' => 'required|min:2',
            'user_lname' => 'required|min:2',
//            'password' => 'required|string|min:6|confirmed'

        ];


        if ($request['user_task'] == "update") {

            $validation = Validator::make($request, $rules);
            if ($validation->fails()) {

                return redirect()->action('UsersController@edit', ['id' => $request['user_id']])->withErrors($validation->errors())->withInput();

            }

            if ($request['togglId'] != 0)

                $toggl_id = $request['togglId'];
            else
                $toggl_id = $request['toggl_id'];

            if ($request['workspaceId'] != 0)

                $workspace_id = $request['workspaceId'];
            else
                $workspace_id = $request['workspace_id'];

            if ($request['git_username_id'] != 0) {

                preg_match_all('/[^\s]+/', $request['git_username_id'], $match);

                $git_id = $match[0][0];
                $git_username = $match[0][1];

            } else {
                $git_username = $request['git_username'];
                $git_id = $request['git_id'];
            }

            User::where('user_id', $request['user_id'])->update([

                'user_fname' => $request['user_fname'],
                'user_lname' => $request['user_lname'],
                'git_username' => $git_username,
                'user_email' => $request['user_email'],
                'toggl_id' => $toggl_id,
                'toggl_api' => $request['toggl_api'],
                'git_id' => $git_id,
                'git_key' => $request['git_key'],
                'fk_for_workspace' => $workspace_id

            ]);
            if ($request['user_role'] != User::find($request['user_id'])->roles()->first()->id) {
                $user = User::find($request['user_id']);
                $user->detachRoles();
                $role = Role::where('id', $request['user_role'])->first();
                $user->attachRole($role);

            }

            return redirect()->action('UsersController@index');

        } else {
            $rules = [

                'user_email' => 'required|email|unique:users',
                'password' => 'required|string|min:6|confirmed'

            ];

            $validation = Validator::make($request, $rules);

            if ($validation->fails()) {

                return redirect()->action('UsersController@add')->withErrors($validation->errors())->withInput();

            }

//            if ($request['togglId'] != "")

                $id_toggl = $request['togglId'];

//            else $id_toggl = $request['toggl_id'];



                preg_match_all('/[^\s]+/', $request['git_username_id'], $match);

                $git_id = $match[0][0];
                $git_username = $match[0][1];




            DB::table('users')->insert([

                'user_fname' => $request['user_fname'],
                'user_lname' => $request['user_lname'],
                'git_username' => $git_username,
                'user_email' => $request['user_email'],
                'toggl_id' => $id_toggl,
                'toggl_api' => $request['toggl_api'],
                'git_id' => $git_id,
                'git_key' => $request['git_key'],
                'password' => bcrypt($request['password']),
                'fk_for_workspace' => $request['workspaceId']

            ]);

            $user = User::where('user_email', $request['user_email'])->first();
            $role = Role::where('id', $request['user_role'])->first();
            $user->attachRole($role);

            return redirect()->action('UsersController@add');
        }

    }

    public
    function delete()
    {
        $request = Request::all();

        User::where('user_id', $request['user_id'])->delete();

        return redirect()->action('UsersController@index');

    }

    public
    function add()
    {

        $worspaces = Workspace::all();
        $toggl_users_array = [];
        $i = 0;
        $j = 0;
        $roles = Role::all();

        $workspacesAll = [];
        $numeration = 0;
        $usernames = [];


        $git_usernames = Curl::to('https://gitlab.com/api/v4/groups/80635/members')
            ->withHeader('Private-Token: d7j9soN_FEYvya4rsABL')
            ->withData(array('owned' => 'true'))
            ->asJson()
            ->get();

        foreach ($git_usernames as $git_username) {
            if (!Helperss::in_array_field($git_username->id, 'git_id', $usernames, true)) {


                if (User::where('git_id', $git_username->id)->count() == 0) {

                    $usernames[$j]['id'] = $git_username->id;
                    $usernames[$j]['name'] = $git_username->username;
                    $j++;
                }
            }
        }

        foreach ($worspaces as $workspace) {

            $workspacesAll[$numeration]['id'] = $workspace->w_id;
            $workspacesAll[$numeration]['name'] = $workspace->workspace_name;
            $numeration++;


            $togglservice = new TogglService($workspace->workspace_id, $workspace->toggl_token);

            $variables = $togglservice->projectsUsers($workspace->workspace_id);

            foreach ($variables as $variable) {
                if (!Helperss::in_array_field($variable->id, 'toggl_id', $toggl_users_array, true)) {


                    if (User::where('toggl_id', $variable->id)->count() == 0) {

                        $toggl_users_array[$i]['toggl_id'] = $variable->id;
                        $toggl_users_array[$i]['full_name'] = $variable->fullname;
                        $toggl_users_array[$i]['email'] = $variable->email;
                        $i++;
                    }
                }
            }
        }

        return view('users.add', compact('toggl_users_array', 'roles', 'workspacesAll', 'usernames'));

    }

    public
    function profile()
    {


        return view('users.profile');
    }

    public
    function editPassword()
    {

        $request = Request::all();

        $rules = [

            'password' => 'required|string|min:6|confirmed'

        ];

        $validation = Validator::make($request, $rules);
        if ($validation->fails()) {

            return redirect()->action('UsersController@edit', ['id' => $request['user_id']])->withErrors($validation->errors())->withInput();

        }

        User::where('user_id', $request['user_id'])->update([

            'password' => bcrypt($request['password'])

        ]);

        return redirect()->action('UsersController@edit', ['id' => $request['user_id']]);

    }


}

