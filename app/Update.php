<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Update extends Model
{
    protected $primaryKey="update_id";
    protected $table="updates";

    public function timeEntryEntity(){

        return  $this->hasMany('App\TimeEntry','fk_for_update','update_id');
    }
}
