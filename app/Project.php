<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
protected $primaryKey="project_id";
protected $table='projects';
protected $guarded = [];

    public function togglProjectEntity(){

     return $this->hasMany('App\TogglProject','fk_for_project','project_id');

    }

    public function timeEntryEntity(){

        return $this->hasMany('App\TimeEntry','fk_for_projects','project_id');
    }
}
