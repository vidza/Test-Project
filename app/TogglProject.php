<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TogglProject extends Model
{
    protected $primaryKey='tp_id';
    protected $table='toggl_projects';
    protected $guarded = [];


    public function workspaceEntity(){

    return $this->hasOne('App\Workspace','w_id','fk_for_workspace');

    }

    public function projectEntity(){

        return $this->belongsTo('App\Project','project_id','fk_for_project');

        }

}
