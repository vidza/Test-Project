<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Workspace extends Model
{
    protected $primaryKey='w_id';
    protected $table='workspace';

    public function togglEntity(){


       return $this->belongsTo('App\TogglProject','fk_for_workspace','w_id');

    }
}
