<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{

    use Notifiable;
    use EntrustUserTrait;


    protected $primaryKey='user_id';
    protected $table='users';

    protected $guarded = [];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function timeEntryEntity(){

        return $this->hasMany('App\TimeEntry','fk_for_user','user_id');

    }

}
