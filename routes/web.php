<?php
Route::get('/', 'HomeController@index')->name('home');
Route::get('projects','ProjectsController@index');
Route::get('users','UsersController@index');
Route::get('reports','ReportsController@index');


Route::get('users/add','UsersController@add');
Route::get('users/edit/{id}','UsersController@edit');
Route::get('users/profile','UsersController@profile');


Route::get('projects/add','ProjectsController@add');
Route::get('projects/edit/{id}','ProjectsController@edit');


Route::get('reports/show','ReportsController@show');
Route::get('reports/all', 'ReportsController@showAll');
Route::get('reports/filters', 'ReportsController@showAll');
Route::get('reports/filters/tabs','ReportsController@partialFilter');
Route::get('reports/updategit/all','ReportsController@updateAllOnGit');
Route::get('reports/show/entriesview','ReportsController@showEntries');


Route::get('workspace','WorkspaceController@index');
Route::get('workspace/add','WorkspaceController@add');
Route::get('workspace/edit/{id}','WorkspaceController@edit');


Route::post('users','UsersController@store');
Route::post('users/add','UsersController@store');
Route::post('users/delete','UsersController@delete');
Route::post('users/editPassword','UsersController@editPassword');


Route::post('reports','ReportsController@store');
Route::post('reports/store','ReportsController@store');
Route::post('reports/show','ReportsController@selectedEntries');
Route::post('reports/show/updategit','ReportsController@updateGit');
Route::post('reports/filters/tabs','ReportsController@partialFilter');
Route::post('reports/show/entriesview','ReportsController@showEntries');

Route::post('reports/gitissue','ReportsApiController@glCreateIssue');

Route::post('logout','LogoutController@logout');


Route::post('projects','ProjectsController@sync');
Route::post('projects/add','ProjectsController@create');
Route::post('projects/edit','ProjectsController@store');
Route::post('projects/delete','ProjectsController@delete');


Route::post('workspace/edit','WorkspaceController@update');
Route::post('workspace/add','WorkspaceController@store');
Route::post('workspace/delete','WorkspaceController@delete');





Route::get('hello', function(){

    $date2= new \DateTime();

    $date2->createFromFormat('Y-m-d\TH:i:sT*','2018-02-02T11:40:08+00:00');



    return view('hello',compact('date3'));
});



Auth::routes();

