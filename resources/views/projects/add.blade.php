@extends('layout')

@section('title')
    Add | Project
@endsection

@section('headline')
    Create New Project
@endsection
{!! Form::macro('gitWorkspace',function ($name,$git_workspace,$selected=null,$options=null){
        $i=0;
        $list[$i]="";
    foreach ($git_workspace as $workspace)

	$list[++$i]=[$workspace['id'] =>$workspace['name']];

    $options=['class'=>'form-control'];

	return $this->select($name, $list,$selected,$options);

}) !!}
{!! Form::macro('togglProjects',function ($name,$toggl_project,$selected=null,$options=null){
        $i=0;
    foreach ($toggl_project as $project)

	$list[++$i]=[$project['project_id'].' '.$project['workspace_id'] => $project['project_name'].' '.$project['project_id'].' '.$project['workspace_name']];

    $options=['class'=>'form-control'];

	return $this->select($name, $list,$selected,$options);

}) !!}

{!! Form::macro('createProject',function ($name,$repositories,$selected=null,$options=[]){
    $i=0;
    $list=null;
    foreach ($repositories as $repository)

	$list[++$i]=[$repository['name'] => $repository['name']];

    $options=['class'=>'form-control', 'style' =>'width: 100%','data-placeholder' =>'Choose repository', 'multiple' => 'multiple','id'=>'select2'];
    $selected=null;

	return $this->select($name, $list,$selected,$options);

}) !!}{!! Form::macro('chooseWorkspace',function ($name,$workspaces,$selected=null,$options=[]){
    $i=0;

    foreach ($workspaces as $workspace)

	$list[++$i]=[$workspace['workspace_name'] => $workspace['workspace_name']];

    $options=['class'=>'form-control', 'style' =>'width: 100%','data-placeholder' =>'Choose Toggl Workspace', 'multiple' => 'multiple','id'=>'select2'];
    $selected=null;

	return $this->select($name, $list,$selected,$options);

}) !!}




@section('active-menu')
    <li class="nav-parent active">
        <a href="">
            <i class="glyphicon glyphicon-book"></i>
            <span>Projects</span>
        </a>
        <ul class="children">
            <li>
                <a href="{{url(('projects'))}}">List all Projects</a>
            </li>
            <li class="active">
                <a href="{{url(('projects/add'))}}">Add a Project</a>
            </li>

        </ul>
    </li>
    <li class="nav-parent">
        <a href="">
            <i class="glyphicon glyphicon-user"></i>
            <span>Users</span>
        </a>
        <ul class="children">
            <li>
                <a href="{{url(('users'))}}">List all Users</a>
            </li>
            <li>
                <a href="{{url(('users/add'))}}">Add new User</a>
            </li>

        </ul>
    </li>
    <li class="nav-parent">
        <a href="">
            <i class="glyphicon glyphicon-tasks"></i>
            <span>Reports</span>
        </a>
        <ul class="children">
            <li>
                <a href="{{url(('reports/filters'))}}">List all Reports</a>
            </li>
            <li>
                <a href="{{url(('reports'))}}">Sync Reports</a>
            </li>
        </ul>
    </li>

    <li class="nav-parent">
        <a href="">
            <i class="glyphicon glyphicon-globe"></i>
            <span>Workspaces</span>
        </a>
        <ul class="children">
            <li>
                <a href="{{url(('workspace'))}}">List all Workspaces</a>
            </li>
            <li>
                <a href="{{url(('workspace/add'))}}">Add a Workspace</a>
            </li>

        </ul>
    </li>
@endsection

@section('content')
    <ol class="breadcrumb breadcrumb-quirk">
        <li>
            <a href="{{url('/')}}">
                <i class="glyphicon glyphicon-home"></i>
                Home
            </a>
        </li>
        <li class="active">
            <a href="{{url('projects/add')}}">
                <i class="glyphicon glyphicon-folder-open"></i>
                Add new
                Project
            </a>
        </li>
    </ol>

    <div class="panel">
        <div class="panel-heading">
            <div class="col-lg-5">
            </div>
            <div class="col-lg-7"><h4 class="panel-title">Add new Project</h4>
            </div>
        </div>
        <br>
        <div class="panel-body">
            {{Form::open(['redirect'=>'projects/add', 'class' => 'form-horizontal'])}}

            <div class="form-group">
                {{Form::label('project_name','Project Name: ',['class'=>'col-lg-2 control-label'])}}
                <div class="col-lg-5">
                    @if($errors->has('project_name'))

                        {{Form::text('project_name',Input::old('project_name'),['class'=>'form-control','placeholder'=>'Enter a project name'])}}
                        @foreach($errors->get('project_name') as $error)
                            <br> <p class="alert alert-warning">{!! $error !!}</p>
                        @endforeach
                    @else
                        {{Form::text('project_name',Input::old('project_name'),['class'=>'form-control has-error','placeholder'=>'Enter a project name'])}}
                    @endif
                </div>
            </div>
            <div class="form-group">
                {{Form::label('repository','Create project in:',['class'=>'col-lg-2 control-label'])}}
                <div class="col-lg-5">
                    {{Form::createProject('repository[]',$repositories)}}
                </div>

            </div>
            <div class="form-group">
                {{Form::label('workspace','Choose workspace: ',['class'=>'col-lg-2 control-label'])}}
                <div class="col-lg-5">
                    {{Form::chooseWorkspace('workspace[]',$workspaces)}}

                </div>
            </div>
            <div class="form-group">
                {!! Form::label('gitWorkspaceId','Choose Git Workspace:',['class'=>'col-lg-2 control-label']) !!}
                <div class="col-lg-5">
                    {!! Form::gitWorkspace('gitWorkspaceId',$git_workspace) !!}
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('togglProject','Choose from existing Toggl Project:',['class'=>'col-lg-2 control-label']) !!}
                <div class="col-lg-5">
                    {!! Form::togglProjects('togglProject',$toggl_project) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-5">
                    {{Form::reset('Reset',['class'=>'btn btn-default'])}}
                </div>
                <div class="col-lg-3">
                    <a href="{{url("projects")}}">{{Form::button('Cancel',['class' =>'btn btn-warning'])}}</a>
                    {{Form::submit('Create Project',['class'=>'btn btn-primary'])}}
                </div>

                {{Form::close()}}
            </div>
        </div>
    </div>

@endsection
