@extends('layout')

@section('title')
    Edit|Projects
@endsection

@section('headline')
    Edit Project:
@endsection



{!! Form::macro('buttonToggl',function($booleans,$toggl_project_id=null,$content=false,$options=null){

    if($booleans['empty_toggl']){

    $name = 'toggl_project_id';
    $options=['class'=>'tgl tgl-flip','id'=>'toggl'];

    return $this->checkbox($name,$toggl_project_id,$content,$options);

    }

    else{

        $name='toggl_project_id';
        $options=['class' =>'form-control','disabled' => 'disabled'];
     return $this->text($name,$toggl_project_id,$options);

        }
    })
!!}


{!! Form::macro('buttonGit',function($booleans,$selected_project=null,$content=false,$options=null){

    if($booleans['empty_git']){

    $name = 'git_project_id';
    $options=['class'=>'tgl tgl-flip','id'=>'git'];
    $content=false;
    return $this->checkbox($name,$selected_project,$content,$options);
    }

    else{

    $name='git_project_id';
    $options=['class' =>'form-control','disabled' => 'disabled'];
     return $this->text($name,$selected_project,$options);

        }
    })
!!}


{!! Form::macro('gitProjects',function ($name,$project_array,$selected=null,$options=array()){
        $i=0;
        $list[$i]=" ";
    foreach ($project_array as $project)

	$list[++$i]=[$project['git_project_id'] => $project['project_name'].' '.$project['git_project_id']];
    $options=['class ' => 'form-control','id' =>'sel1'];

	return $this->select($name, $list,$selected=null,$options);

}) !!}


{!! Form::macro('togglProjects',function ($name,$toggl_project,$selected=null,$options=null){
        $i=0;
        $list[$i]="";
    foreach ($toggl_project as $project)

	$list[++$i]=[$project['project_id'].' '.$project['workspace_id'] => $project['project_name'].' '.$project['project_id'].' '.$project['workspace_name']];

    $options=['class'=>'form-control'];

	return $this->select($name, $list,$selected,$options);

}) !!}


{!! Form::macro('chooseWorkspace',function ($name,$free_workspace,$selected=null,$options=[]){
    $i=0;
    $list=null;
    foreach ($free_workspace as $workspace)

	$list[++$i]=[$workspace['workspace_name'] => $workspace['workspace_name']];

    $options=['class'=>'form-control', 'style' =>'width: 100%','data-placeholder' =>'Choose Toggle Workspace', 'multiple' => 'multiple','id'=>'select2'];
    $selected=null;

	return $this->select($name, $list,$selected,$options);

}) !!}



@section('active-menu')
    <li class="nav-parent active">
        <a href="">
            <i class="glyphicon glyphicon-book"></i>
            <span>Projects</span>
        </a>
        <ul class="children">
            <li>
                <a href="{{url(('projects'))}}">List all Projects</a>
            </li>
            <li>
                <a href="{{url(('projects/add'))}}">Add a Project</a>
            </li>

        </ul>
    </li>
    <li class="nav-parent">
        <a href="">
            <i class="glyphicon glyphicon-user"></i>
            <span>Users</span>
        </a>
        <ul class="children">
            <li>
                <a href="{{url(('users'))}}">List all Users</a>
            </li>
            <li>
                <a href="{{url(('users/add'))}}">Add new User</a>
            </li>

        </ul>
    </li>
    <li class="nav-parent">
        <a href="">
            <i class="glyphicon glyphicon-tasks"></i>
            <span>Reports</span>
        </a>
        <ul class="children">
            <li>
                <a href="{{url(('reports/filters'))}}">List all Reports</a>
            </li>
            <li>
                <a href="{{url(('reports'))}}">Sync Reports</a>
            </li>

        </ul>
    </li>
    <li class="nav-parent">
        <a href="">
            <i class="glyphicon glyphicon-globe"></i>
            <span>Workspaces</span>
        </a>
        <ul class="children">
            <li>
                <a href="{{url(('workspace'))}}">List all Workspaces</a>
            </li>
            <li>
                <a href="{{url(('workspace/add'))}}">Add a Workspace</a>
            </li>

        </ul>
    </li>

@endsection



@section('content')
    <ol class="breadcrumb breadcrumb-quirk">

        <li>
            <a href="{{url('/')}}">
                <i class="fa fa-home mr5"></i>
                Home
            </a>
        </li>
        <li>
            <a href="{{url('projects')}}">
                <i class="glyphicon glyphicon-book"></i>
                All Projects
            </a>
        </li>
        <li class="active">
            <a
                    href="{{url('projects/edit',$selected_project['project_id'])}}">
                <i class="glyphicon glyphicon-folder-open"></i> {{ $selected_project['project_name']}}</a>
        </li>
    </ol>

    <div class="panel">

        <div class="panel-heading">
            <div class="col-lg-5">
            </div>
            <div class="col-lg-7">
                <h4 class="panel-title">Edit project: "{{$selected_project['project_name']}}"</h4>
            </div>
        </div>
        <br>
        <div class="panel-body">
            {{Form::open(['url'=>'projects/edit','class' =>'form-horizontal'])}}

            {!! Form::hidden('project_id', $selected_project['project_id']) !!}

            <div class="form-group">
                {{Form::label('name','Project name: ',['class'=>'col-lg-2 control-label'])}}
                <div class="col-lg-5">

                    @if($errors->has('project_name'))

                        {{Form::text('project_name',Input::old('project_name'),['class'=>'form-control','placeholder'=>'Enter a project name'])}}

                        @foreach($errors->get('project_name') as $error)
                            <br> <p class="alert alert-warning">{!! $error !!}</p>
                        @endforeach
                    @else
                        {{Form::text('project_name',$selected_project['project_name'],['class'=>'form-control'])}}
                </div> @endif
            </div>
            <div class="form-group">
                @if($booleans['empty_toggl'])
                    {{Form::label('togglid','Create or choose existing Toggl project:  ',['class'=>'col-lg-2 control-label'])}}

                @else {{Form::label('togglid','Toggl ID: ',['class'=>'col-lg-2 control-label'])}}

                @endif
                <div class="col-lg-2">
                    {{Form::buttonToggl($booleans,$toggl_project->toggl_project_id)}}
                    @if($booleans['empty_toggl'])

                        {!! Form::label('toggl','',['class'=>'tgl-btn','data-tg-off'=> 'Create','data-tg-on'=>'Undo', 'style' =>'width:80%']) !!}
                    @endif
                </div>
                @if($booleans['empty_toggl'])
                    <div class="col-sm-3">
                        {{Form::togglProjects('togglProject',$togglP)}}
                    </div>
                @endif
            </div>
            <div class="form-group">
                @if(count($free_workspace) >0)
                    {{Form::label('workspace','Choose workspace: ',['class'=>'col-lg-2 control-label'])}}
                @endif
                <div class="col-lg-5">
                    {{Form::chooseWorkspace('workspace[]',$free_workspace)}}
                </div>
            </div>

            <div class="form-group">
                @if(!empty($toggl_project->workspace_id))

                    {{Form::label('workspace','Workspace: ',['class'=>'col-lg-2 control-label'])}}
                    <div class="col-lg-5">
                        {{Form::text('workspace_id',$toggl_project->workspace_id,['class'=>'form-control'])}}
                    </div>
                @endif
            </div>

            <div class="form-group">

                @if ($booleans['empty_git'])
                    {{Form::label('chooseGit','Create or choose existing Git Project:',['class'=>'col-lg-2 control-label'])}}

                @else  {{Form::label('gitid','Git ID: ',['class'=>'col-lg-2 control-label'])}}

                @endif
                <div class="col-lg-2">
                    {{Form::buttonGit($booleans,$selected_project['git_project_id'])}}
                    @if($booleans['empty_git'])
                        {!! Form::label('git','',['class'=>'tgl-btn','data-tg-off'=> 'Create','data-tg-on'=>'Undo','style' =>'width:80%']) !!}
                    @endif
                </div>
                @if($booleans['empty_git'])
                    <div class="col-sm-3">
                        {{Form::gitProjects('sync_git_project_id',$git_project_array)}}
                    </div>
                @endif
            </div>

            <div class="form-group">
                <div class="col-lg-5">
                    {{Form::reset('Revert',['class'=>'btn btn-default'])}}
                </div>
                <div class="col-lg-3">
                    <a href="{{url("projects")}}">{{Form::button('Cancel',['class' =>'btn btn-warning'])}}</a>
                    {{Form::submit('Edit Project',['class' =>'btn btn-primary'])}}
                </div>

                {{Form::close()}}
            </div>
        </div>
    </div>

@endsection


