@extends('layout')

@section('title')
    Projects
@endsection

@section('headline')
    Projects
@endsection

@section('active-menu')
    <li class="nav-parent active">
        <a href="">
            <i class="glyphicon glyphicon-book"></i>
            <span>Projects</span>
        </a>
        <ul class="children">
            <li class="active">
                <a href="{{url(('projects'))}}">List all Projects</a>
            </li>
            <li>
                <a href="{{url(('projects/add'))}}">Add a Project</a>
            </li>

        </ul>
    </li>
    <li class="nav-parent">
        <a href="">
            <i class="glyphicon glyphicon-user"></i>
            <span>Users</span>
        </a>
        <ul class="children">
            <li>
                <a href="{{url(('users'))}}">List all Users</a>
            </li>
            <li>
                <a href="{{url(('users/add'))}}">Add new User</a>
            </li>

        </ul>
    </li>
    <li class="nav-parent">
        <a href="">
            <i class="glyphicon glyphicon-tasks"></i>
            <span>Reports</span>
        </a>
        <ul class="children">

            <li>
                <a href="{{url(('reports/filters'))}}">List all Reports</a>
            </li>
            <li>
                <a href="{{url(('reports'))}}">Sync Reports</a>
            </li>

        </ul>
    </li>

    <li class="nav-parent">
        <a href="">
            <i class="glyphicon glyphicon-globe"></i>
            <span>Workspaces</span>
        </a>
        <ul class="children">
            <li>
                <a href="{{url(('workspace'))}}">List all Workspaces</a>
            </li>
            <li>
                <a href="{{url(('workspace/add'))}}">Add a Workspace</a>
            </li>

        </ul>
    </li>

@endsection
@section('content')

    <ol class="breadcrumb breadcrumb-quirk">
        <li>
            <a href="{{url('/')}}">
                <i class="glyphicon glyphicon-home"></i>
                Home
            </a>
        </li>
        <li class="active">
            <a href="{{url('projects')}}">
                <i class="glyphicon glyphicon-book"></i>
                All Projects
            </a>
        </li>
    </ol>
    <div class="panel">
        <div class="panel-heading">
            <div class="col-lg-5">
            </div>
            <div class="col-lg-7"><h4 class="panel-title">List of all Projects</h4>
            </div>
        </div>
        <br>
        <div class="panel-body">
            <div class="table-responsive">
                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead class="bg-primary">
                        <tr>
                            <td>Project name</td>
                            <td>Project toggl ID</td>
                            <td>Workspace</td>
                            <td>Gitlab ID</td>
                            <td>Actions</td>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($projects as $project)

                            <tr>

                                <td>{{$project['project_name']}}</td>
                                @if (empty($project['toggl_id']))
                                    <td>{{''}}</td>
                                @else
                                    <td>{{$project['toggl_id']}}</td>
                                @endif
                                @if (empty($project['workspace_name']))
                                    <td>{{''}}</td>
                                @else
                                    <td>{{$project['workspace_name']}}</td>
                                @endif
                                @if (empty($project['git_project_id']))
                                    <td>{{''}}</td>
                                @else
                                    <td>{{$project['git_project_id']}}</td>
                                @endif

                                <td style="width: 120px;">
                                    <a href="{{url('projects/edit',$project['project_id'])}}" class="btn btn-warning"
                                       role="button">Edit
                                    </a>
                                    <a data-toggle="modal" href="#myModal" class="btn btn-danger" id="deleteProject"
                                       data-project={{$project['project_id']}}
                                               role="button">Delete
                                    </a>
                                </td>

                            </tr>
                    @endforeach
                    <tbody>
                </table>
            </div>
        </div>
    </div>


    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Notification</h4>
                </div>
                <div class="modal-body">
                    Are you sure you want to delete this Project?
                </div>
                <div class="modal-footer">
                    <div class="col-lg-1">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                    </div>
                    <div class="col-lg-3">
                    </div>

                    <div class="col-lg-4">
                        {!! Form::open(['url'=> 'projects/delete']) !!}
                        {!! Form::hidden('all') !!}
                        {!! Form::hidden('project_id',null,['class'=>'project_id']) !!}
                        {!! Form::submit('Delete from Git/Toggl/Giggl',['class'=>'btn btn-danger']) !!}
                        {!! Form::close(); !!}
                    </div>

                    <div class="col-lg-3">
                        {!! Form::open(['url'=> 'projects/delete']) !!}
                        {!! Form::hidden('giggl') !!}
                        {!! Form::hidden('project_id',null,['class'=>'project_id']) !!}
                        {!! Form::submit('Delete only from Giggl',['class'=>'btn btn-success']) !!}
                        {!! Form::close(); !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection



