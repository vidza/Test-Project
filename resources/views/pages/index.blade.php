@extends('layout')

@section('title')
    Giggl
@endsection

@section('headline')
    Home page
@endsection
@section('active-menu')
    @if(Entrust::hasRole('admin'))
        <li class="nav-parent">
            <a href="">
                <i class="fa glyphicon glyphicon-book"></i>
                <span>Projects</span>
            </a>
            <ul class="children">
                <li>
                    <a href="{{url(('projects'))}}">List all Projects</a>
                </li>
                <li>
                    <a href="{{url(('projects/add'))}}">Add a Project</a>
                </li>

            </ul>
        </li>
    @endif

    @if(Entrust::hasRole('admin'))


        <li class="nav-parent">
            <a href="">
                <i class="glyphicon glyphicon-user"></i>
                <span>Users</span>
            </a>
            <ul class="children">
                <li>
                    <a href="{{url(('users'))}}">List all Users</a>
                </li>
                <li>
                    <a href="{{url(('users/add'))}}">Add new User</a>
                </li>

            </ul>
        </li>
    @endif
    @if(Entrust::hasRole('admin'))
    <li class="nav-parent">
        <a href="">
            <i class="glyphicon glyphicon-tasks"></i>
            <span>Reports</span>
        </a>
        <ul class="children">
            <li>
                <a href="{{url(('reports/filters'))}}">List all Reports</a>
            </li>
            <li>
                <a href="{{url(('reports'))}}">Sync Reports</a>
            </li>

        </ul>
    </li>
    @endif
    <li class="nav-parent">
        <a href="">
            <i class="glyphicon glyphicon-globe"></i>
            <span>Workspaces</span>
        </a>
        <ul class="children">
            <li>
                <a href="{{url(('workspace'))}}">List all Workspaces</a>
            </li>
            <li>
                <a href="{{url(('workspace/add'))}}">Add a Workspace</a>
            </li>

        </ul>
    </li>

@endsection
@section('content')

    <ol class="breadcrumb breadcrumb-quirk">

        <li>
            <a href="{{url('/')}}">
                <i class="glyphicon glyphicon-home"></i>
                Home
            </a>
        </li>

    </ol>

    <div class="row panel-quick-page">
        <a href="{{url('workspace')}}">
            <div class="col-xs-4 col-sm-5 col-md-4 page-support">
                <div class="panel">
                    <div class="panel-heading">
                        <h4 class="panel-title">Manage Workspaces</h4>
                    </div>
                    <div class="panel-body">
                        <div class="page-icon">
                            <i class="glyphicon glyphicon-globe"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <a href="{{url('projects')}}">
            <div class="col-xs-4 col-sm-4 col-md-4 page-user">
                <div class="panel">
                    <div class="panel-heading">
                        <h4 class="panel-title">Manage Projects</h4>
                    </div>
                    <div class="panel-body">
                        <div class="page-icon">
                            <i class="glyphicon glyphicon-book"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <a href="{{url('users')}}">
            <div class="col-xs-4 col-sm-4 col-md-4 page-reports">
                <div class="panel" id="users">
                    <div class="panel-heading">
                        <h4 class="panel-title">Manage Users</h4>
                    </div>
                    <div class="panel-body">
                        <div class="page-icon">
                            <i class="fa fa-users"></i>
                        </div>
                    </div>
                </div>

            </div>
        </a>
        <a href="{{url('workspace/add')}}">
            <div class="col-xs-4 col-sm-4 col-md-4 page-messages">
                <div class="panel">
                    <div class="panel-heading">
                        <h4 class="panel-title">Add new Workspace</h4>
                    </div>
                    <div class="panel-body">
                        <div class="page-icon">
                            <i class="glyphicon glyphicon-cloud-upload"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <a href="{{url('projects/add')}}">
            <div class="col-xs-4 col-sm-4 col-md-4 page-events">
                <div class="panel">
                    <div class="panel-heading">
                        <h4 class="panel-title">Add New Project</h4>
                    </div>
                    <div class="panel-body">
                        <div class="page-icon">
                            <i class="glyphicon glyphicon-folder-open"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <a href="{{url('users/add')}}">
            <div class="col-xs-4 col-sm-4 col-md-4 page-statistics">
                <div class="panel">
                    <div class="panel-heading">
                        <h4 class="panel-title">Add New User</h4>
                    </div>
                    <div class="panel-body">
                        <div class="page-icon">
                            <i class="fa fa-user-plus"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <div class="col-xs-4 col-sm-4 col-md-4 page-statistics">

        </div>
        <a href="{{url('reports/updategit/all')}}">
            <div class="col-xs-4 col-sm-4 col-md-4 page-button" style="margin-top: 10%;width: 20%;margin-left:7%">
                <div class="panel">
                    <div class="panel-heading">
                        <h4 class="panel-title" style="text-align: center">Toggl -> GitLab</h4>
                    </div>
                    <div class="panel-body">
                        <div class="page-icon">
                            <i class="glyphicon glyphicon-save"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>


@endsection

