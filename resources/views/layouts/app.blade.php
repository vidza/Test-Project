<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('css/quirk.css')}}">
</head>
<body>
<div class="headerpanel">

    <!-- logopanel -->


    <div class="headerbar">

        <div class="header-right">

            <ul class="headermenu">

                <li>
                    <div class="logopanel">
                        <a href="{{url(('/'))}}">
                            <img src="{{asset('images/giggl-text.png')}}" alt="" class="media-object img-square"
                                 style="width:30%;height:34px; margin-left: -650px">
                        </a>
                    </div>
                </li>
                <li>
                    <div class="collapse navbar-collapse" id="app-navbar-collapse">
                        <!-- Left Side Of Navbar -->
                        <ul class="nav navbar-nav">
                            &nbsp;
                        </ul>

                        <!-- Right Side Of Navbar -->
                        <ul class="nav navbar-nav navbar-right">
                            <!-- Authentication Links -->
                            @guest
                                <li><a href="{{ route('login') }}">Login</a></li>
                                <li><a href="{{ route('register') }}">Register</a></li>

                            @endguest
                        </ul>
                    </div>
                </li>
            </ul>
        </div><!-- header-right -->
    </div><!-- headerbar -->
</div><!-- header-->
</header>

    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">

        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
