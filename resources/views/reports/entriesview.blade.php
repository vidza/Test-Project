<div id="replaced-entries">

    @foreach($time_array as $key1=>$project)

        @foreach($project as $key2=> $name)

            <div class="panel">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-lg-10">
                            <h4 class="panel-title">Project:</h4>
                            <p>{{$key1}}</p>
                        </div>

                    </div>
                    <br>
                    <div class="row">

                        <div class="col-lg-3">
                            <h4 class="panel-title">User:</h4>

                            <p>{{$key2}} </p>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="example table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead class="bg-primary">

                            <tr>
                                <td>Start time</td>
                                <td>Description</td>
                                <td>Duration</td>
                                <td>Connection to GitLab</td>
                                <td style="width:50px;">Add custom tag</td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($name as $key=>$time_entries)

                                <tr>

                                    <td>{{$time_entries->start_time}}</td>
                                    <td>{{$time_entries->description}}</td>
                                    <td>{{$time_entries->duration}}</td>
                                    <td style="width:200px;">
                                        @if($time_entries->status_git)
                                            {{--{!! Form::checkbox('delete_issue[]',$time_entries->time_entry_id,false,['data-toggle'=>'toggle','data-onstyle' =>'warning','data-offstyle' =>'danger','data-on'=>'Undo','data-off'=>'Delete']) !!}--}}
                                        @else {!! Form::checkbox('add_issue[]',$time_entries->time_entry_id,false,['class'=>'tgl tgl-flip add','id'=>$time_entries->time_entry_id])!!}
                                        {!! Form::label($time_entries->time_entry_id,'',['class'=>'tgl-btn glyphicon glyphicon-remove','data-tg-off'=> 'Add','data-tg-on'=>'Waiting']) !!}
                                        @endif

                                        @if($time_entries->status_git)
                                            {!! Form::button('Updated',['class' =>'btn btn-success glyphicon glyphicon-saved','val','style' =>'width:100%']) !!}
                                        @else {!! Form::button('Not updated',['class' =>'btn btn-danger glyphicon glyphicon-remove']) !!}
                                        @endif
                                    </td>
                                    @if($time_entries->status_git)
                                        <td>{{Form::text('tags','',['class'=>'form-control tag','placeholder'=>'for example #1 or #2...','data-id'=>'text', 'disabled'=>'disabled'])}}</td>
                                    @else
                                        <td>{{Form::text('tags','',['class'=>'form-control tag','placeholder'=>'for example #1 or #2...','data-id'=>'text','data-value'=>$time_entries->time_entry_id])}}</td>


                                    @endif
                                </tr>

                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        @endforeach
    @endforeach

</div>