@extends('layout')

@section('title')
    Reports
@endsection

@section('headline')
    Reports
@endsection
{{--{!! Form::macro('addIssue',function ($name,$users_array,$selected=null,$options=[]){
    $i=0;
    $list=null;
    foreach ($users_array as $user)

	$list[++$i]=[$user['user_id'] => $user['user_fname'].' '.$user['user_lname']];

    $options=['class'=>'form-control', 'style' =>'width: 100%','data-placeholder' =>'Choose a user', 'multiple' => 'multiple','id'=>'select2'];
    $selected=null;

	return $this->select($name, $list,$selected,$options);

}) !!}--}}


@section('active-menu')
    <li class="nav-parent">
        <a href=""><i class="glyphicon glyphicon-book"></i> <span>Projects</span></a>
        <ul class="children">
            <li><a href="{{url(('projects'))}}">List all Projects</a></li>
            <li><a href="{{url(('projects/add'))}}">Add a Project</a></li>

        </ul>
    </li>
    <li class="nav-parent"><a href=""><i class="glyphicon glyphicon-user"></i> <span>Users</span></a>
        <ul class="children">
            <li><a href="{{url(('users'))}}">List all Users</a></li>
            <li><a href="{{url(('users/add'))}}">Add new User</a></li>

        </ul>
    </li>
    <li class="nav-parent active"><a href=""><i class="glyphicon glyphicon-tasks"></i> <span>Reports</span></a>
        <ul class="children">
            <li class="active">
                <a href="{{url(('reports/filters'))}}">List all Reports</a>
            </li>
            <li>
                <a href="{{url(('reports'))}}">Sync Reports</a>
            </li>

        </ul>
    </li>
    <li class="nav-parent">
        <a href="">
            <i class="glyphicon glyphicon-globe"></i>
            <span>Workspaces</span>
        </a>
        <ul class="children">
            <li>
                <a href="{{url(('workspace'))}}">List all Workspaces</a>
            </li>
            <li>
                <a href="{{url(('workspace/add'))}}">Add a Workspace</a>
            </li>

        </ul>
    </li>

@endsection

@section('content')
    <ol class="breadcrumb breadcrumb-quirk">
        <li><a href="{{url('/')}}"><i class="fa fa-home mr5"></i> Home</a></li>
        <li class="active"><a href="{{url('reports')}}"><i class="glyphicon glyphicon-tasks"></i> All Reports</a></li>
    </ol>

    <div class="row" id="page">
        {!! Form::open(['url'=>'reports/all/tabs','method' => 'post']) !!}
        {{--{!! Form::hidden('entries','showAll') !!}--}}

        <div class="col-lg-3">
            <div class="form-group">
                <div class="btn-group">
                    <button type="button" class="btn btn-def dropdown-toggle" data-toggle="dropdown">Projects
                        <span class="caret"></span></button>
                    <ul class="dropdown-menu scrollable-menu" role="menu">

                        @foreach($projects as $project)

                            <li><a href="#" class="small" data-value="option1"
                                   tabIndex="-1">{{Form::checkbox('project[]',$project->project_id,false,['class' =>'control'])}}{{$project->project_name}}</a>
                            </li>

                        @endforeach
                    </ul>
                </div>
                <div class="btn-group">
                    <button type="button" class="btn btn-def dropdown-toggle" data-toggle="dropdown">Users
                        <span class="caret"></span></button>
                    <ul class="dropdown-menu scrollable-menu" role="menu">

                        @foreach($users as $user)

                            <li><a href="#" class="small" data-value="option1"
                                   tabIndex="-1">{{Form::checkbox('user[]',$user->user_id,false)}}{{$user->user_fname.' '.$user->user_lname}}</a>
                            </li>

                        @endforeach
                    </ul>
                </div>
                <div class="btn-group">
                    <button type="button" class="btn btn-def dropdown-toggle" data-toggle="dropdown">Workspaces
                        <span class="caret"></span></button>
                    <ul class="dropdown-menu scrollable-menu" role="menu">

                        @foreach($workspaces as $workspace)

                            <li><a href="#" class="small" data-value="option1"
                                   tabIndex="-1">{{Form::checkbox('workspace[]',$workspace->workspace_id,false)}}{{$workspace->workspace_name}}</a>
                            </li>

                        @endforeach
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-lg-9">
            <div class="form-group">

                <div class="col-lg-3">

                    <div class="btn-group checkRadioContainer">
                        <button type="button" class="btn btn-def dropdown-toggle groupedBy" data-toggle="dropdown">
                            Grouped
                            by
                            <span
                                    class="caret"></span></button>
                        <ul class="dropdown-menu scrollable-menu" role="menu">

                            <li><h6 class="dropdown-header">Group by</h6></li>
                            <li class="hover-drop"><a href="#" class="small group-by" data-value="option1"
                                                      tabIndex="-1"><label id="project"><input type="radio"
                                                                                               name="group-by"
                                                                                               class="groupBy"
                                                                                               value="groupByProjects"
                                                                                               checked/><i
                                                class="fa fa-check fa-2x"></i>Projects</label></a></li>
                            <li class="hover-drop"><a href="#" class="small group-by" data-value="option1"
                                                      tabIndex="-1"><label id="user"><input type="radio"
                                                                                            name="group-by"
                                                                                            class="groupBy"
                                                                                            value="groupByUsers"/><i
                                                class="fa fa-check fa-2x"></i>Users</label></a></li>
                            <li class="hover-drop"><a href="#" class="small group-by" data-value="option1"
                                                      tabIndex="-1"><label id="workspace"><input type="radio"
                                                                                                 name="group-by"
                                                                                                 class="groupBy"
                                                                                                 value="groupByWorkspace"/><i
                                                class="fa fa-check fa-2x"></i>Workspace</label></a></li>

                            <li><h6 class="dropdown-header">Subgroup by</h6></li>
                            <li class="hover-drop"><a href="#" class="small sub-group-by" data-value="option1"
                                                      tabIndex="-1"><label><input type="radio"
                                                                                  name="sub-group-by"
                                                                                  class="subGroupBy"
                                                                                  value="timeEntry"/><i
                                                class="fa fa-check fa-2x"></i>Time Entry</label></a></li>
                            <li class="hover-drop"><a href="#" class="small sub-group-by" data-value="option1"
                                                      tabIndex="-1"><label><input type="radio"
                                                                                  name="sub-group-by"
                                                                                  class="subGroupBy"
                                                                                  value="project"/><i
                                                class="fa fa-check fa-2x"></i>Project</label></a></li>
                            <li class="hover-drop"><a href="#" class="small sub-group-by" data-value="option1"
                                                      tabIndex="-1"><label><input type="radio"
                                                                                  name="sub-group-by"
                                                                                  class="subGroupBy"
                                                                                  value="user" checked/><i
                                                class="fa fa-check fa-2x"></i>Users</label></a></li>


                        </ul>
                    </div>
                </div>
                <div class="col-lg-3">

                    <div class="btn-group checkRadioContainer">
                        <button type="button" class="btn btn-def dropdown-toggle sortedBy" data-toggle="dropdown">
                            Sorted by
                            <span class="caret"></span></button>
                        <ul class="dropdown-menu scrollable-menu" role="menu">

                            <li><h6 class="dropdown-header">Sort by</h6></li>
                            <li class="hover-drop"><a href="#" class="small sort-by" data-value="option1"
                                                      tabIndex="-1"><label><input
                                                type="radio"
                                                name="sort-by"
                                                class="sortBy"
                                                value="title" checked/><i
                                                class="fa fa-check fa-2x"></i>Title</label></a></li>
                            <li class="hover-drop"><a href="#" class="small sort-by" data-value="option1"
                                                      tabIndex="-1"><label><input
                                                type="radio"
                                                name="sort-by"
                                                class="sortBy"
                                                value="duration"/><i
                                                class="fa fa-check fa-2x"></i>Duration</label></a></li>
                            <li><h6 class="dropdown-header">Sort direction</h6></li>
                            <li class="hover-drop"><a href="#" class="small sort-dir" data-value="option2"
                                                      tabIndex="-1"><label><input
                                                type="radio" name="sort-dir" class="sortDir" value="asc"/><i
                                                class="fa fa-check fa-2x"></i>Ascending</label></a>
                            </li>
                            <li class="hover-drop"><a href="#" class="small sort-dir" data-value="option2"
                                                      tabIndex="-1"><label><input
                                                type="radio" name="sort-dir" class="sortDir" value="desc" checked/><i
                                                class="fa fa-check fa-2x"></i>Descending</label></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3">

                    <div class="btn-group checkRadioContainer">
                        <button type="button" class="btn btn-def dropdown-toggle" data-toggle="dropdown">
                            <span class="glyphicon glyphicon-cog"></span><span class="caret"></span></button>
                        <ul class="dropdown-menu scrollable-menu" role="menu">

                            <li class="hover-drop"><a href="#" class="small" data-value="option1"
                                                      tabIndex="-1"><label><input type="radio"
                                                                                  name="time"
                                                                                  value="weekly" checked/><i
                                                class="fa fa-check fa-2x"></i>Weekly</label></a></li>

                            <li class="hover-drop"><a href="#" class="small" data-value="option1"
                                                      tabIndex="-1"><label><input type="radio"
                                                                                  name="time"
                                                                                  value="monthly"/><i
                                                class="fa fa-check fa-2x"></i>Monthly</label></a></li>

                            <li class="hover-drop"><a href="#" class="small" data-value="option1"
                                                      tabIndex="-1"><label><input type="radio"
                                                                                  name="time"
                                                                                  value="summary"/><i
                                                class="fa fa-check fa-2x"></i>Summary</label></a></li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-lg-2">

            {!! Form::submit('Apply',['class' => 'btn btn-success apply-button','id' =>'filter']) !!}
            {!! Form::close() !!}
        </div>
    </div>
    {{-- @if($time_array==null)
         @endif
     <div class="panel" id="nino">--}}
    <div id="filters">
        @if($time_array!=null)
            <div class="col-lg-6">
                <div class="panel">
                    <div class="row">
                        <div class="col-md-11">


                            @foreach($time_array as $key1=>$project)


                                <div class="panel-group" id="accordion">
                                    <div class="panel panel-inverse">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" class="collapsed" aria-expanded="false"
                                                   data-parent="#accordion" href="#{{$key1}}">
                                                    {{$key1}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="{{$key1}}" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                @foreach($project as $key2=> $name)
                                                    <br>
                                                    <div class="col-lg-10"> {{$key2}}
                                                    </div>
                                                    <div class="col-lg-2">{{\App\Helperss\Helperss::convertSum($sum_array[$key1])}}</div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            @endforeach

                        </div>

                    </div>

                </div>
            </div>
    </div>

    @endif
@endsection
