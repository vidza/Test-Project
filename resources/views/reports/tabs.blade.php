
<div class="panel">
    <div class="row">
        <div class="col-md-6">

            <h4 class="panel-title mb5">Projects</h4>
            {{--<p class="mb15">Get base styles and flexible support for collapsible components like accordions and navigation.</p>--}}
            @foreach($time_array as $key1=>$project)


                <div class="panel-group" id="accordion">
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse"  class="collapsed" aria-expanded="false" data-parent="#accordion" href="#{{$key1}}">
                                    {{$key1}}
                                </a>
                            </h4>
                        </div>
                        <div id="{{$key1}}" class="panel-collapse collapse">
                            <div class="panel-body">
                                @foreach($project as $key2=> $name)
                                    <br>
                                    <div class="col-lg-10"> {{$key2}}
                                    </div>
                                    <div class="col-lg-2">{{\App\Helperss\Helperss::convertSum($sum_array[$key1])}}</div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                </div>

            @endforeach

        </div>

    </div>

</div>