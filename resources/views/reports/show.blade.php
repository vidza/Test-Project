@extends('layout')

@section('title')
    Reports
@endsection

@section('headline')
    Reports
@endsection
{{--{!! Form::macro('addIssue',function ($name,$users_array,$selected=null,$options=[]){
    $i=0;
    $list=null;
    foreach ($users_array as $user)

	$list[++$i]=[$user['user_id'] => $user['user_fname'].' '.$user['user_lname']];

    $options=['class'=>'form-control', 'style' =>'width: 100%','data-placeholder' =>'Choose a user', 'multiple' => 'multiple','id'=>'select2'];
    $selected=null;

	return $this->select($name, $list,$selected,$options);

}) !!}--}}


@section('active-menu')
    <li class="nav-parent">
        <a href=""><i class="glyphicon glyphicon-book"></i> <span>Projects</span></a>
        <ul class="children">
            <li><a href="{{url(('projects'))}}">List all Projects</a></li>
            <li><a href="{{url(('projects/add'))}}">Add a Project</a></li>

        </ul>
    </li>
    <li class="nav-parent"><a href=""><i class="glyphicon glyphicon-user"></i> <span>Users</span></a>
        <ul class="children">
            <li><a href="{{url(('users'))}}">List all Users</a></li>
            <li><a href="{{url(('users/add'))}}">Add new User</a></li>

        </ul>
    </li>
    <li class="nav-parent active"><a href=""><i class="glyphicon glyphicon-tasks"></i> <span>Reports</span></a>
        <ul class="children">
            <li class="active">
                <a href="{{url(('reports/filters'))}}">List all Reports</a>
            </li>
            <li>
                <a href="{{url(('reports'))}}">Sync Reports</a>
            </li>

        </ul>
    </li>
    <li class="nav-parent">
        <a href="">
            <i class="glyphicon glyphicon-globe"></i>
            <span>Workspaces</span>
        </a>
        <ul class="children">
            <li>
                <a href="{{url(('workspace'))}}">List all Workspaces</a>
            </li>
            <li>
                <a href="{{url(('workspace/add'))}}">Add a Workspace</a>
            </li>

        </ul>
    </li>

@endsection

@section('content')
    <ol class="breadcrumb breadcrumb-quirk">
        <li><a href="{{url('/')}}"><i class="fa fa-home mr5"></i> Home</a></li>
        <li class="active"><a href="{{url('reports')}}"><i class="glyphicon glyphicon-tasks"></i> All Reports</a></li>
    </ol>

        {!! Form::open(['url' => 'reports/show/updategit', 'method' => 'post']) !!}
<div id="entries">

        @foreach($time_array as $key1=>$project)

            @foreach($project as $key2=> $name)

                <div class="panel">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-lg-10">
                                <h4 class="panel-title">Project:</h4>
                                <p>{{$key1}}</p>
                            </div>

                        </div>
                        <br>
                        <div class="row">

                            <div class="col-lg-3">
                                <h4 class="panel-title">User:</h4>

                                <p>{{$key2}} </p>
                            </div>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="example table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead class="bg-primary">

                                <tr>
                                    <td>Start time</td>
                                    <td>Description</td>
                                    <td>Duration</td>
                                    <td>Connection to GitLab</td>
                                    <td style="width:50px;">Add custom tag</td>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($name as $key=>$time_entries)

                                    <tr>

                                        <td>{{$time_entries->start_time}}</td>
                                        <td>{{$time_entries->description}}</td>
                                        <td>{{$time_entries->duration}}</td>
                                        <td style="width:200px;">
                                            @if($time_entries->status_git)
                                                {{--{!! Form::checkbox('delete_issue[]',$time_entries->time_entry_id,false,['data-toggle'=>'toggle','data-onstyle' =>'warning','data-offstyle' =>'danger','data-on'=>'Undo','data-off'=>'Delete']) !!}--}}
                                            @else {!! Form::checkbox('add_issue[]',$time_entries->time_entry_id,false,['class'=>'tgl tgl-flip add','id'=>$time_entries->time_entry_id])!!}
                                            {!! Form::label($time_entries->time_entry_id,'',['class'=>'tgl-btn glyphicon glyphicon-remove','data-tg-off'=> 'Add','data-tg-on'=>'Waiting']) !!}
                                            @endif

                                            @if($time_entries->status_git)
                                                {!! Form::button('Updated',['class' =>'btn btn-success glyphicon glyphicon-saved','val','style' =>'width:100%']) !!}
                                            @else {!! Form::button('Not updated',['class' =>'btn btn-danger glyphicon glyphicon-remove']) !!}
                                            @endif
                                        </td>
                                        @if($time_entries->status_git)
                                            <td>{{Form::text('tags','',['class'=>'form-control tag','placeholder'=>'for example #1 or #2...','data-id'=>'text', 'disabled'=>'disabled'])}}</td>
                                        @else
                                            <td>{{Form::text('tags','',['class'=>'form-control tag','placeholder'=>'for example #1 or #2...','data-id'=>'text','data-value'=>$time_entries->time_entry_id])}}</td>


                                        @endif
                                    </tr>

                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            @endforeach
        @endforeach

            </div>

            <div class="col-lg-5" id="hidden-fields">
        </div>
            <div class="col-lg-3">

            {!! Form::submit('Sync with GitLab',['class'=>'btn btn-primary']) !!}
            {!! Form::close() !!}
        </div>

@endsection
