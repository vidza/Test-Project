@extends('layout')

@section('title')
    Reports
@endsection

@section('headline')
    Reports
@endsection


{!! Form::macro('showUsers',function ($name,$users_array,$selected=null,$options=[]){
    $i=0;
    $list=null;
    foreach ($users_array as $user)

	$list[++$i]=[$user['user_id'] => $user['user_fname'].' '.$user['user_lname']];

    $options=['class'=>'form-control', 'style' =>'width: 100%','data-placeholder' =>'Choose a user', 'multiple' => 'multiple','id'=>'select2'];
    $selected=null;

	return $this->select($name, $list,$selected,$options);

}) !!}

@section('active-menu')
    <li class="nav-parent">
        <a href=""><i class="glyphicon glyphicon-book"></i> <span>Projects</span></a>
        <ul class="children">
            <li><a href="{{url(('projects'))}}">List all Projects</a></li>
            <li><a href="{{url(('projects/add'))}}">Add a Project</a></li>

        </ul>
    </li>
    <li class="nav-parent"><a href=""><i class="glyphicon glyphicon-user"></i> <span>Users</span></a>
        <ul class="children">
            <li><a href="{{url(('users'))}}">List all Users</a></li>
            <li><a href="{{url(('users/add'))}}">Add new User</a></li>

        </ul>
    </li>
    <li class="nav-parent active"><a href=""><i class="glyphicon glyphicon-tasks"></i> <span>Reports</span></a>
        <ul class="children">
            <li>
                <a href="{{url(('reports/filters'))}}">List all Reports</a>
            </li>
            <li class="active">
                <a href="{{url(('reports'))}}">Sync Reports</a>
            </li>

        </ul>
    </li>
    <li class="nav-parent">
        <a href="">
            <i class="glyphicon glyphicon-globe"></i>
            <span>Workspaces</span>
        </a>
        <ul class="children">
            <li>
                <a href="{{url(('workspace'))}}">List all Workspaces</a>
            </li>
            <li>
                <a href="{{url(('workspace/add'))}}">Add a Workspace</a>
            </li>

        </ul>
    </li>

@endsection

@section('content')
    <ol class="breadcrumb breadcrumb-quirk">
        <li><a href="{{url('/')}}"><i class="glyphicon glyphicon-home"></i> Home</a></li>
        <li class="active"><a href="{{url('reports')}}"><i class="glyphicon glyphicon-tasks"></i> Sync Reports</a></li>
    </ol>
   <div class="panel">
       <div class="panel-heading">
           <div class="col-lg-5">
           </div>
           <div class="col-lg-7">
               <h4 class="panel-title">List of All Reports</h4>

           </div>
       </div>
       <br>
    <div class="panel-body">
        <div class="row">
            {!! Form::open(['action' =>['ReportsController@store'],'class'=> 'form-horizontal']) !!}

            {!! Form::hidden('update', 'update_entries') !!}
            <div class="col-lg-12">
                <div class="form-group">
                    {!! Form::submit('Update time entries for every user',['class'=>'btn btn-warning']) !!}

                    {!! Form::label('last_update','Last update: '.$update_array['time_of_update'],['class'=>'col-lg-2 control-label']) !!}
                    {!! Form::close(); !!}
                </div>
            </div>

        </div>

<br>
<br>
    {!! Form::open(['action' =>['ReportsController@selectedEntries']]) !!}

    <div class="row">

        <div class="col-sm-3">
            <div class="input-group">

                <input type="text" class="form-control datepicker" placeholder="mm/dd/yyyy" name="start_date">

                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="input-group">
                <input type="text" class="form-control datepicker" placeholder="mm/dd/yyyy" name="end_date">
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
            </div>
        </div>
        <div class="col-sm-2"> {{--space--}}</div>


        <div class="col-sm-4">

            {!! Form::showUsers('user[]',$user_array) !!}

        </div>
    </div>
<br>
            <div class="table-responsive">
                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%" color="red">

                    <thead>
                    <tr>
                        <th>Name</th>
                        <th style="width: 60px;position:center;text-align:center">Show</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($project_array as $project)
                        <tr>


                            <td> {!! Form::label($project['project_name']) !!}</td>
                            <td><label class="switch">{!! Form::checkbox('project[]', $project['project_id'],false, ['class'=> 'switch',]) !!} <span class="slider round"></span>
                                </label></td>

                    @endforeach
                    </tbody>
                </table>
                <br>
                <div class="form-group">
                    <div class="col-lg-5">
                    </div>

                    <div class="col-lg-3">

                        {!! Form::submit('Show selected Entries',['class'=>'btn btn-primary']) !!}
                        {{Form::close()}}
                    </div>

                </div>
                <br>
                <br>
            </div>
        </div>

    </div>




@endsection

