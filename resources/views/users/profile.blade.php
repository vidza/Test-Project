@extends('layout')

@section('title')
    My Profile
@endsection

@section('headline')
    Home page
@endsection
@section('active-menu')
    <li class="nav-parent">
        <a href=""><i class="fa glyphicon glyphicon-book"></i> <span>Projects</span></a>
        <ul class="children">
            <li><a href="{{url(('projects'))}}">List all Projects</a></li>
            <li><a href="{{url(('projects/add'))}}">Add a Project</a></li>

        </ul>
    </li>
    <li class="nav-parent"><a href=""><i class="glyphicon glyphicon-user"></i> <span>Users</span></a>
        <ul class="children">
            <li ><a href="{{url(('users'))}}">List all Users</a></li>
            <li><a href="{{url(('users/add'))}}">Add new User</a></li>

        </ul>
    </li>
    <li class="nav-parent"><a href=""><i class="glyphicon glyphicon-tasks"></i> <span>Reports</span></a>
        <ul class="children">
            <li><a href="{{url(('reports'))}}">All Reports</a></li>

        </ul>
    </li>

@endsection
@section('content')
    <ol class="breadcrumb breadcrumb-quirk">
        <li><a href="{{url('/')}}"><i class="fa fa-home mr5"></i> Home</a></li>
        <li class="active"><a href="{{url('users')}}"><i class="fa fa-users"></i> All Users</a></li>
    </ol>




    @endsection