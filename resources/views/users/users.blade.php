@extends('layout')

@section('title')
    Users
@endsection

@section('headline')
    Users
@endsection


@section('active-menu')
    <li class="nav-parent">
        <a href="">
            <i class="glyphicon glyphicon-book"></i>
            <span>Projects</span>
        </a>
        <ul class="children">
            <li>
                <a href="{{url(('projects'))}}">List all Projects</a>
            </li>
            <li>
                <a href="{{url(('projects/add'))}}">Add a Project</a>
            </li>

        </ul>
    </li>
    <li class="nav-parent active">
        <a href="">
            <i class="glyphicon glyphicon-user"></i>
            <span>Users</span>
        </a>
        <ul class="children">
            @if(Entrust::hasRole('admin'))
                <li class="active">
                    <a href="{{url(('users'))}}">List all Users</a>
                </li>
            @endif
            <li>
                <a href="{{url(('users/add'))}}">Add new User</a>
            </li>

        </ul>
    </li>
    <li class="nav-parent">
        <a href="">
            <i class="glyphicon glyphicon-tasks"></i>
            <span>Reports</span>
        </a>
        <ul class="children">
            <li>
                <a href="{{url(('reports/filters'))}}">List all Reports</a>
            </li>
            <li>
                <a href="{{url(('reports'))}}">Sync Reports</a>
            </li>

        </ul>
    </li>
    <li class="nav-parent">
        <a href="">
            <i class="glyphicon glyphicon-globe"></i>
            <span>Workspaces</span>
        </a>
        <ul class="children">
            <li>
                <a href="{{url(('workspace'))}}">List all Workspaces</a>
            </li>
            <li>
                <a href="{{url(('workspace/add'))}}">Add a Workspace</a>
            </li>

        </ul>
    </li>

@endsection

@section('content')
    <ol class="breadcrumb breadcrumb-quirk">
        <li>
            <a href="{{url('/')}}">
                <i class="fa fa-home mr5"></i>
                Home
            </a>
        </li>
        <li class="active">
            <a href="{{url('users')}}">
                <i class="fa fa-users"></i>
                All Users
            </a>
        </li>
    </ol>
    <div class="panel">
        <div class="panel-heading">
            <div class="col-lg-5">
            </div>
            <div class="col-lg-7">
                <h4 class="panel-title">List of All Users</h4>
            </div>
        </div>
        <br>
        <div class="panel-body">
            <div class="table-responsive">
                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead class="bg-primary">
                        <tr>
                            <td>First name</td>
                            <td>Last name</td>
                            <td>Gitlab Username</td>
                            <td>Email</td>
                            <td>Role</td>
                            <td>Toggl Workspace</td>
                            <td >Toggl ID</td>
                            <td>Toggl API key</td>
                            <td>GitLab ID</td>
                            <td>GitLab Key</td>
                            <td>Actions</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($user as $users)
                            <tr>
                                <td>{{$users['user_fname']}}</td>
                                <td>{{$users['user_lname']}}</td>
                                <td>{{$users['git_username']}}</td>
                                <td>{{$users['user_email']}}</td>
                                @if($users->HasRole('admin'))
                                    <td style="width:15px">{!! Form::button($users->roles()->first()->name,['class' =>'btn .btn-sm btn-success role-width']) !!}</td>
                                @else
                                    <td style="width:15px">{!! Form::button($users->roles()->first()->name,['class' =>'btn .btn-sm btn-default role-width']) !!}</td>
                                @endif
                                <td >{{$workspace_name[$numeration++]}}</td>
                                <td >{{$users['toggl_id']}}</td>
                                <td>{{$users['toggl_api']}}</td>
                                <td>{{$users['git_id']}}</td>
                                <td>{{$users['git_key']}}</td>
                                <td style="width: 150px;">
                                    <a href="{{url('users/edit',$users['user_id'])}}"
                                       class="edit btn btn-warning" role="button">Edit
                                    </a>
                                    <a data-toggle="modal" href="#myModal" class="btn btn-danger" id="deleteUser"
                                       data-user={{$users['user_id']}}
                                               role="button">Delete
                                    </a>
                            </tr>

                    @endforeach

                    <tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Notification</h4>
                </div>
                <div class="modal-body">
                    Are you sure you want to delete this User?
                </div>
                <div class="modal-footer">
                    <div class="col-lg-1">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                    </div>
                    <div class="col-lg-3">
                    </div>

                    <div class="col-lg-4">

                    </div>

                    <div class="col-lg-3">
                        {!! Form::open(['url'=> 'users/delete']) !!}
                        {!! Form::hidden('user_id',null,['class'=>'user_id']) !!}
                        {!! Form::submit('Yes, I am sure',['class'=>'btn btn-success']) !!}
                        {!! Form::close(); !!}
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
