@extends('layout')

@section('title')
    Edit|User
@endsection

@section('headline')
    Edit User
@endsection


{!! Form::macro('togglUsers',function ($name,$toggl_array,$selected=null,$options=null){
        $i=0;
        $list[$i]="";
    foreach ($toggl_array as $user)

	$list[++$i]=[$user['toggl_id'] => $user['full_name'].' '.$user['email'].' '.$user['toggl_id']];
    $options=['class'=>'form-control'];

	return $this->select($name, $list,$selected,$options);

}) !!}

{!! Form::macro('userRoles',function ($name,$roles,$selected=null,$options=null){
        $i=0;
    foreach ($roles as $role)
	$list[++$i]=[$role->id=> $role->name];
    $options=['class'=>'form-control'];

	return $this->select($name, $list,$selected,$options);

}) !!}{!! Form::macro('userWorkspace',function ($name,$workspaces,$selected=null,$options=null){
        $i=0;
        $list[$i]="";
    foreach ($workspaces as $workspace)
	$list[++$i]=[$workspace['id'] => $workspace['name']];
    $options=['class'=>'form-control'];

	return $this->select($name, $list,$selected,$options);

}) !!}
{!! Form::macro('gitUsername',function ($name,$usernames,$selected=null,$options=null){
        $i=0;
        $list[$i]="";
    foreach ($usernames as $username)
	$list[++$i]=[$username['id'].' '.$username['name']=> $username['name'].' '.$username['id']];
    $options=['class'=>'form-control'];

	return $this->select($name, $list,$selected,$options);

}) !!}
@section('active-menu')
    <li class="nav-parent active">
        <a href="">
            <i class="glyphicon glyphicon-book"></i>
            <span>Projects</span>
        </a>
        <ul class="children">
            <li>
                <a href="{{url(('projects'))}}">List all Projects</a>
            </li>
            <li>
                <a href="{{url(('projects/add'))}}">Add a Project</a>
            </li>

        </ul>
    </li>
    <li class="nav-parent">
        <a href="">
            <i class="glyphicon glyphicon-user"></i>
            <span>Users</span>
        </a>
        <ul class="children">
            <li>
                <a href="{{url(('users'))}}">List all Users</a>
            </li>
            <li>
                <a href="{{url(('users/add'))}}">Add new User</a>
            </li>

        </ul>
    </li>
    <li class="nav-parent">
        <a href="">
            <i class="glyphicon glyphicon-tasks"></i>
            <span>Reports</span>
        </a>
        <ul class="children">
            <li>
                <a href="{{url(('reports/filters'))}}">List all Reports</a>
            </li>
            <li>
                <a href="{{url(('reports'))}}">Sync Reports</a>
            </li>

        </ul>
    </li>
    <li class="nav-parent">
        <a href="">
            <i class="glyphicon glyphicon-globe"></i>
            <span>Workspaces</span>
        </a>
        <ul class="children">
            <li>
                <a href="{{url(('workspace'))}}">List all Workspaces</a>
            </li>
            <li>
                <a href="{{url(('workspace/add'))}}">Add a Workspace</a>
            </li>

        </ul>
    </li>

@endsection


@section('content')
    <ol class="breadcrumb breadcrumb-quirk">
        <li>
            <a href="{{url('/')}}">
                <i class="fa fa-home mr5"></i>
                Home
            </a>
        </li>
        <li>
            <a href="{{url('users')}}">
                <i class="fa fa-users"></i>
                All Users
            </a>
        </li>
        <li class="active">
            <a
                    href="{{url('users/edit',$selected_user['user_id'])}}">
                <i
                        class="glyphicon glyphicon-user"></i> {{ $selected_user['user_fname'].' '.$selected_user['user_lname']}}
            </a>
        </li>
    </ol>

    {{--url->redirect--}}
    <div class="panel">
        <div class="panel-heading">
            <div class="col-lg-5">
            </div>
            <div class="col-lg-7"><h4 class="panel-title">
                    Edit User: {{$selected_user['user_fname'].' '.$selected_user['user_lname']}}</h4>
            </div>
        </div>
        <br>
        <div class="panel-body">

            {!! Form::open(['url'=>'users','class'=>'form-horizontal']); !!}

            {!! Form::hidden('user_id', $selected_user['user_id']) !!}
            {!! Form::hidden('user_task', 'update') !!}

            <div class="form-group">
                {!! Form::label('user_fname','First name',['class'=>'col-lg-1 control-label']); !!}
                <div class="col-lg-5">

                    @if($errors->has('user_fname'))

                        {!! Form::text('user_fname',Input::old('user_fname'),['class'=>'form-control has-error','placeholder'=>'Enter your first name']); !!}

                        @foreach($errors->get('user_fname') as $error)
                            <br> <p class="alert alert-warning">{!! $error !!}</p>
                        @endforeach
                    @else
                        {!! Form::text('user_fname',$selected_user['user_fname'],['class'=>'form-control','placeholder'=>'Enter your first name']); !!}
                    @endif
                </div>
            </div>

            <div class="form-group">

                {!! Form::label('user_lname','Last name',['class'=>'col-lg-1 control-label']); !!}
                <div class="col-lg-5">
                    @if($errors->has('user_lname'))
                        {!! Form::text('user_lname',Input::old('user_lname'),['class'=>'form-control has-error','placeholder'=>'Enter your last name']); !!}
                        @foreach($errors->get('user_lname') as $error)
                            <br> <p class="alert alert-warning">{!! $error !!}</p>
                        @endforeach
                    @else
                        {!! Form::text('user_lname',$selected_user['user_lname'],['class'=>'form-control','placeholder'=>'Enter your last name']); !!}
                    @endif
                </div>
            </div>
            <div class="form-group">

                {!! Form::label('git_username','Gitlab Username',['class'=>'col-lg-1 control-label']); !!}
                <div class="col-lg-2">
                    @if(empty($selected_user['git_username']))

                        {!! Form::text('git_username',$selected_user['git_username'],['class'=>'form-control','placeholder'=>'Gitlab Username']); !!}
                        @else
                        {!! Form::text('git_username',$selected_user['git_username'],['class'=>'form-control','placeholder'=>'Gitlab Username','readonly'=>'readonly']); !!}
                    @endif
                </div>
                <div class="col-lg-3">
                    {!! Form::gitUsername('git_username_id',$usernames) !!}
                </div>
            </div>

            <div class="form-group">

                {!! Form::label('user_email','E-Mail',['class'=>'col-lg-1 control-label']); !!}
                <div class="col-lg-5">
                    @if($errors->has('user_email'))
                        {{ Form::email('user_email',Input::old('user_email'),['class'=>'form-control has-error','placeholder'=>'Enter your e-mail address']) }}
                        @foreach($errors->get('user_email') as $error)
                            <br> <p class="alert alert-warning">{!! $error !!}</p>
                        @endforeach
                    @else
                        {!! Form::email('user_email',$selected_user['user_email'],['class'=>'form-control','placeholder'=>'Enter your e-mail address']); !!}
                    @endif
                </div>
            </div>

            <div class="form-group">

                {!! Form::label('workspace_name','Workspace',['class'=>'col-lg-1 control-label']); !!}
                <div class="col-lg-2">
                    @if(empty($workspace->workspace_name))
                    {!! Form::text('workspace_name',$workspace->workspace_name,['class'=>'form-control','placeholder'=>'Enter your Toggl ID']); !!}
                    @else
                        {!! Form::text('workspace_name',$workspace->workspace_name,['class'=>'form-control','placeholder'=>'Enter your Toggl ID','readonly' =>'readonly']); !!}
                    @endif
                        {!! Form::hidden('workspace_id',$workspace->w_id) !!}
                </div>
                <div class="col-lg-3">
                    {!! Form::userWorkspace('workspaceId',$workspaces) !!}
                </div>
            </div>
            <div class="form-group">

                {!! Form::label('toggl_id','Toggl ID',['class'=>'col-lg-1 control-label']); !!}
                <div class="col-lg-2">
                    @if(empty($selected_user['toggl_id']))

                    {!! Form::text('toggl_id',$selected_user['toggl_id'],['class'=>'form-control','placeholder'=>'Enter your Toggl ID']); !!}
                        @else
                        {!! Form::text('toggl_id',$selected_user['toggl_id'],['class'=>'form-control','placeholder'=>'Enter your Toggl ID','readonly' =>'readonly']); !!}
                        @endif
                </div>
                <div class="col-lg-3">
                    {!! Form::togglUsers('togglId',$toggl_users_array) !!}
                </div>
            </div>

            <div class="form-group">

                {!! Form::label('toggl_api','Toggl API key',['class'=>'col-lg-1 control-label']); !!}
                <div class="col-lg-5">
                    {!! Form::text('toggl_api',$selected_user['toggl_api'],['class'=>'form-control','placeholder'=>'Enter your Toggl Api key']); !!}
                </div>
            </div>

            <div class="form-group">

                {!! Form::label('git_id','Gitlab ID',['class'=>'col-lg-1 control-label']); !!}
                <div class="col-lg-5">
                    {!! Form::text('git_id',$selected_user['git_id'],['class'=>'form-control','placeholder'=>'Enter your Gitlab ID','readonly' =>'readonly']); !!}
                </div>
            </div>

            <div class="form-group">

                {!! Form::label('git_key','Gitlab API Key',['class'=>'col-lg-1 control-label']); !!}
                <div class="col-lg-5">
                    {!! Form::text('git_key',$selected_user['git_key'],['class'=>'form-control', 'placeholder'=>'Enter your Gitlab API key']); !!}
                </div>
            </div>
            <div class="form-group">

                {!! Form::label('user_role','User role',['class'=>'col-lg-1 control-label']); !!}
                <div class="col-lg-5">
                    {!! Form::userRoles('user_role',$roles,$role) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-5">
                    {{Form::reset('Revert',['class'=>'btn btn-default'])}}
                </div>
                <div class="col-lg-3">
                    <a href="{{url("users")}}">{{Form::button('Cancel',['class' =>'btn btn-warning'])}}</a>
                    {!! Form::submit('Edit User',['class' => 'btn btn-primary']); !!}
                    {{Form::close()}}
                </div>
                <div class="col-lg-4">

                    <a data-toggle="modal" href="#myModal" class="btn btn-danger" id="editPassword"
                       data-user={{$selected_user['user_id']}}
                               role="button">Change Password
                    </a>
                </div>


            </div>


        </div>
    </div>

    {!! Form::open(['url'=> 'users/editPassword']) !!}
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Notification</h4>
                </div>
                <div class="modal-body" style="height: 150px">
                    <div class="form-group form-horizontal">
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                            <label for="password" class="col-lg-3 control-label">Password</label>

                            <div class="col-lg-8">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-lg-3 control-label">Confirm Password</label>

                            <div class="col-lg-8">
                                <input id="password-confirm" type="password" class="form-control"
                                       name="password_confirmation" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-lg-5">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                    </div>
                    <div class="col-lg-3">
                    </div>

                    <div class="col-lg-4">

                    </div>

                    <div class="col-lg-3">

                        {!! Form::hidden('user_id',null,['class'=>'user_id']) !!}
                        {!! Form::submit('Save Password',['class'=>'btn btn-success']) !!}
                        {!! Form::close(); !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
