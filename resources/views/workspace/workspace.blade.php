@extends('layout')

@section('title')
    Workspaces
@endsection

@section('headline')
     Workspace
@endsection

@section('active-menu')
    @if(Entrust::hasRole('admin'))
        <li class="nav-parent">
            <a href="">
                <i class="fa glyphicon glyphicon-book"></i>
                <span>Projects</span>
            </a>
            <ul class="children">
                <li>
                    <a href="{{url(('projects'))}}">List all Projects</a>
                </li>
                <li>
                    <a href="{{url(('projects/add'))}}">Add a Project</a>
                </li>

            </ul>
        </li>
    @endif

    @if(Entrust::hasRole('admin'))


        <li class="nav-parent">
            <a href="">
                <i class="glyphicon glyphicon-user"></i>
                <span>Users</span>
            </a>
            <ul class="children">
                <li>
                    <a href="{{url(('users'))}}">List all Users</a>
                </li>
                <li>
                    <a href="{{url(('users/add'))}}">Add new User</a>
                </li>

            </ul>
        </li>
    @endif
    <li class="nav-parent">
        <a href="">
            <i class="glyphicon glyphicon-tasks"></i>
            <span>Reports</span>
        </a>
        <ul class="children">
            <li>
                <a href="{{url(('reports/filters'))}}">List all Reports</a>
            </li>
            <li>
                <a href="{{url(('reports'))}}">Sync Reports</a>
            </li>

        </ul>
    </li>
    <li class="nav-parent">
        <a href="">
            <i class="glyphicon glyphicon-globe"></i>
            <span>Workspaces</span>
        </a>
        <ul class="children">
            <li class="active">
                <a href="{{url(('workspace'))}}">List all Workspaces</a>
            </li>
            <li>
                <a href="{{url(('workspace/add'))}}">Add a Workspace</a>
            </li>

        </ul>
    </li>

@endsection

@section('content')
    <ol class="breadcrumb breadcrumb-quirk">
        <li>
            <a href="{{url('/')}}">
                <i class="glyphicon glyphicon-home"></i>
                Home
            </a>
        </li>
        <li>
            <a href="{{url('workspace/add')}}">
                <i class="glyphicon glyphicon-globe"></i>
                All Workspaces
            </a>
        </li>
    </ol>

    <div class="panel-body">
        <div class="table-responsive">
            <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead class="bg-primary">
                    <tr>
                        <td>Workspace name</td>
                        <td>Toggl Workspace ID</td>
                        <td>Toggl Token</td>
                        <td>Actions</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($workspaces as $workspace)
                        <tr>
                            <td>{{$workspace->workspace_name}}</td>
                            <td>{{$workspace->workspace_id}}</td>
                            <td>{{$workspace->toggl_token}}</td>
                            <td style="width: 120px;">
                                <a href="{{url('workspace/edit',$workspace->w_id)}}"
                                   class="edit btn btn-warning" role="button">Edit
                                </a>
                                <a data-toggle="modal" href="#myModal" class="btn btn-danger" id="deleteWorkspace"
                                   data-workspace={{$workspace->w_id}}
                                           role="button">Delete
                                </a>
                            </td>

                        </tr>

                @endforeach
                <tbody>


            </table>
        </div>
    </div>
    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Notification</h4>
                </div>
                <div class="modal-body">
                    Are you sure you want to delete this Workspace?
                </div>
                <div class="modal-footer">
                    <div class="col-lg-1">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                    </div>
                    <div class="col-lg-3">
                    </div>

                    <div class="col-lg-4">

                    </div>

                    <div class="col-lg-3">
                        {!! Form::open(['url'=> 'workspace/delete']) !!}
                        {!! Form::hidden('workspace_id',null,['class'=>'workspace']) !!}
                        {!! Form::submit('Yes, I am sure',['class'=>'btn btn-success']) !!}
                        {!! Form::close(); !!}
                    </div>
                </div>
            </div>
        </div>
    </div>






@endsection