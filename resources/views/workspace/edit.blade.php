@extends('layout')

@section('title')
    Edit|Workspace
@endsection

@section('headline')
    Add Workspace
@endsection

@section('active-menu')
    @if(Entrust::hasRole('admin'))
        <li class="nav-parent">
            <a href="">
                <i class="fa glyphicon glyphicon-book"></i>
                <span>Projects</span>
            </a>
            <ul class="children">
                <li>
                    <a href="{{url(('projects'))}}">List all Projects</a>
                </li>
                <li>
                    <a href="{{url(('projects/add'))}}">Add a Project</a>
                </li>

            </ul>
        </li>
    @endif

    @if(Entrust::hasRole('admin'))


        <li class="nav-parent">
            <a href="">
                <i class="glyphicon glyphicon-user"></i>
                <span>Users</span>
            </a>
            <ul class="children">
                <li>
                    <a href="{{url(('users'))}}">List all Users</a>
                </li>
                <li>
                    <a href="{{url(('users/add'))}}">Add new User</a>
                </li>

            </ul>
        </li>
    @endif
    <li class="nav-parent">
        <a href="">
            <i class="glyphicon glyphicon-tasks"></i>
            <span>Reports</span>
        </a>
        <ul class="children">
            <li>
                <a href="{{url(('reports/filters'))}}">List all Reports</a>
            </li>
            <li>
                <a href="{{url(('reports'))}}">Sync Reports</a>
            </li>

        </ul>
    </li>
    <li class="nav-parent">
        <a href="">
            <i class="glyphicon glyphicon-globe"></i>
            <span>Workspaces</span>
        </a>
        <ul class="children">
            <li>
                <a href="{{url(('workspace'))}}">List all Workspaces</a>
            </li>
            <li>
                <a href="{{url(('workspace/add'))}}">Add a Workspace</a>
            </li>

        </ul>
    </li>

@endsection

@section('content')
    <ol class="breadcrumb breadcrumb-quirk">
        <li>
            <a href="{{url('/')}}">
                <i class="glyphicon glyphicon-home"></i>
                Home
            </a>
        </li>
        <li >
            <a href="{{url('workspace/add')}}">
                <i class="glyphicon glyphicon-cloud-upload"></i>
                All Workspaces
            </a>
        </li><li class="active">
            <a href="{{url('workspace/edit',$workspace->w_id)}}">
                <i class="glyphicon glyphicon-cloud-upload"></i>
                All Workspaces
            </a>
        </li>

    </ol>

    <div class="panel">


        <div class="panel-heading">
            <div class="col-lg-5">
            </div>
            <div class="col-lg-7">
                <h4 class="panel-title">Edit "{{$workspace->workspace_name}}" Workspace</h4>
            </div>
        </div>
        <div class="panel-body">


            {!! Form::open(['url'=>'workspace/edit','method'=> 'post']) !!}
            {!! Form::hidden('w_id',$workspace->w_id) !!}
            <div class="form-group">
                <div class="col-lg-1 control-label">
                    {!! Form::label('workspace_name','Toggl Workspace'); !!}
                </div>
                <div class="col-lg-5">
                    @if($errors->has('workspace_name'))

                        {!! Form::text('workspace_name',Input::old('workspace_name'),['class'=>'form-control has-error','placeholder'=>'Enter Toggl Workspace name ...']); !!}

                        @foreach($errors->get('workspace_name') as $error)
                            <br> <p class="alert alert-warning">{!! $error !!}</p>
                        @endforeach
                    @else
                        {!! Form::text('workspace_name',$workspace->workspace_name,['class'=>'form-control','placeholder'=>'Enter workspace name']); !!}
                    @endif
                </div>
            </div>
            <div class="form-group" style="clear: both">
                <div class="col-lg-1 control-label">
                    {!! Form::label('workspace_id','Toggl Workspace ID'); !!}
                </div>
                <div class="col-lg-5">
                    @if($errors->has('workspace_id'))

                        {!! Form::text('workspace_id',Input::old('workspace_id'),['class'=>'form-control has-error','placeholder'=>'Enter Toggl Workspace ID ...']); !!}

                        @foreach($errors->get('workspace_id') as $error)
                            <br> <p class="alert alert-warning">{!! $error !!}</p>
                        @endforeach
                    @else
                        {!! Form::text('workspace_id',$workspace->workspace_id,['class'=>'form-control','placeholder'=>'Enter Toggl Workspace ID ...']); !!}
                    @endif
                </div>
            </div>
            <div class="form-group" style="clear: both">
                <div class="col-lg-1 control-label">
                    {!! Form::label('toggl_token','Toggl Token'); !!}
                </div>
                <div class="col-lg-5">
                    @if($errors->has('toggl_token'))

                        {!! Form::text('toggl_token',Input::old('toggl_token'),['class'=>'form-control has-error','placeholder'=>'Enter Toggl Workspace Token ...']); !!}

                        @foreach($errors->get('toggl_token') as $error)
                            <br> <p class="alert alert-warning">{!! $error !!}</p>
                        @endforeach
                    @else
                        {!! Form::text('toggl_token',$workspace->toggl_token,['class'=>'form-control','placeholder'=>'Enter Workspace Toggl Token ...']); !!}
                    @endif
                </div>
            </div>
            <br>

            <div class="form-group" style="clear: both; margin-top: 20px">
                <div class="col-lg-5">
                    {{Form::reset('Reset',['class'=>'btn btn-default'])}}
                </div>
                <div class="col-lg-3">
                    <a href="{{url("workspace")}}">{{Form::button('Cancel',['class' =>'btn btn-warning'])}}</a>
                    {{Form::submit('Edit Workspace',['class'=>'btn btn-primary'])}}
                </div>
            </div>
            {{Form::close()}}

        </div>
    </div>



@endsection