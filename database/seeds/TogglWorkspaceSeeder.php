<?php

use Illuminate\Database\Seeder;

class TogglWorkspaceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('toggl_workspace')->insert([

            'from_workspace' => 1,
            'from_toggl_project' => 1,

        ]);
        DB::table('toggl_workspace')->insert([

            'from_workspace' => 1,
            'from_toggl_project' => 2,

        ]);
        DB::table('toggl_workspace')->insert([

        'from_workspace' => 1,
        'from_toggl_project' => 3,

    ]);
    }
}
