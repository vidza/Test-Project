<?php

use Illuminate\Database\Seeder;
use App\TimeEntry;

class TimeEntriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('time_entries')->insert([

            'start_time' => '12:00',
            'duration' =>3322,
            'description' => 'time entry',
            'status' => true,
            'fk_for_user' => 1,
            'fk_for_update' => 1
        ]);

    }
}
