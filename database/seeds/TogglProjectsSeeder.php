<?php

use Illuminate\Database\Seeder;

class TogglProjectsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('toggl_projects')->insert([

            'workspace_id' => 17,
            'toggl_project_id' => 2313,
            'fk_for_project' => 1,

        ]);

        DB::table('toggl_projects')->insert([

            'workspace_id' => 456,
            'toggl_project_id' => 112,
            'fk_for_project' => 2,

        ]);
        DB::table('toggl_projects')->insert([

            'workspace_id' => 55,
            'toggl_project_id' => 256,
            'fk_for_project' => 1,

        ]);DB::table('toggl_projects')->insert([

        'workspace_id' => 78,
        'toggl_project_id' => 2,
        'fk_for_project' => 1,

    ]);

    }
}
