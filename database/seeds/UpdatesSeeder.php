<?php

use Illuminate\Database\Seeder;

class UpdatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('updates')->insert([
            'time_of_update'=>\Carbon\Carbon::create(2017,1,1,1,1,1),
            'workspace_id' => '122321',
            'time_of_update_atom' => \Carbon\Carbon::create(2017,1,1,1,1,1)->toAtomString(),
        ]);

    }
}
