<?php

use Illuminate\Database\Seeder;

class ProjectsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('projects')->insert([

           'project_name' => 'First_project',
            'git_project_id' => '123'


        ]);

        DB::table('projects')->insert([

            'project_name' => 'Second_project',
            'git_project_id' => '456'

        ]);
        DB::table('projects')->insert([

            'project_name' => 'Third_project',
            'git_project_id' => '789'

        ]);
    }
}
