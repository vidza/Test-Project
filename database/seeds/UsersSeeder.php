<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([

            'git_id'=> '123',
            'toggl_id'=> '45x',
            'git_key'=> 'a32',
            'user_fname' => 'Nikola',
            'user_lname' => 'Lilic',
            'user_email' => 'lila@gmail.com',

        ]);

        DB::table('users')->insert([

            'git_id'=> '666',
            'toggl_id'=> '999',
            'git_key'=> 'a32xs',
            'user_fname' => 'Mara',
            'user_lname' => 'Tetic',
            'user_email' => 'teta@gmail.com',

        ]);
        DB::table('users')->insert([

            'git_id'=> '432sd',
            'toggl_id'=> 'xxad2',
            'git_key'=> '2323',
            'user_fname' => 'Sara',
            'user_lname' => 'Sokolovic',
            'user_email' => 'saraxxx@gmail.com',

        ]);

    }
}
