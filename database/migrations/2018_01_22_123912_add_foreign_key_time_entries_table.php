<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyTimeEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('time_entries',function(Blueprint $table){
//
            $table->integer('fk_for_projects')->nullable()->unsigned();
            $table->foreign('fk_for_projects')->references('project_id')->on('projects');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('time_entries', function(Blueprint $table)
        {

            $table->dropForeign('toggl_projects_fk_for_projects_foreign');
        });
    }
}
