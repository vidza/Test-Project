<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    protected $fillable=[

        'user_name',
        'user_email'
    ];


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {

            $table->increments('user_id')->unique();
            $table->string('git_id')->nullable();
            $table->string('toggl_id')->nullable();
            $table->string('git_key')->nullable();
            $table->string('user_fname', 40);
            $table->string('user_lname', 40);
            $table->string('user_email');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
