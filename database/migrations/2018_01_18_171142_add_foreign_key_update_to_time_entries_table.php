<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyUpdateToTimeEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('time_entries',function(Blueprint $table){
//            $table->integer('fk_for_update')->unsigned()->nullable();
//            $table->foreign('fk_for_update')->references('update_id')->on('updates');

            $table->integer('fk_for_user')->unsigned()->nullable()->change();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {


    }
}
