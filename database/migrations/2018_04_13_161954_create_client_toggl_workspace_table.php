<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientTogglWorkspaceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_toggl', function (Blueprint $table) {
            $table->integer('client_id')->unsigned();
            $table->integer('toggl_id')->unsigned();
            $table->integer('workspace_id')->unsigned();
            $table->integer('toggl_client_id')->unsigned();

            $table->foreign('client_id')->references('c_id')->on('clients')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('toggl_id')->references('tp_id')->on('toggl_projects')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('workspace_id')->references('w_id')->on('workspace')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['client_id', 'toggl_id','workspace_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_toggl');
    }
}
